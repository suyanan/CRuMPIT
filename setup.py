#!/usr/bin/env python3
from setuptools import setup, find_packages
import glob

install_requires = [
'pymongo',
'ete3',
'tqdm',
'cigar']

setup(
     name='crumpit',
     version='0.1',
     description='Clinincal Real-time Metagenomics Pathogen Identification Test',
     author=['Nick Sanderson'],
     author_email='nicholas.sanderson@ndm.ox.ac.uk',
     scripts=['crumpit/crumpit'],
     packages=find_packages('crumpit'),
     package_dir = {'': 'crumpit'},
     include_package_data=True,
     license='MIT',
     url='https://gitlab.com/ModernisingMedicalMicrobiology/CRuMPIT',
     test_suite='nose.collector',
     tests_require=['nose'],
     install_requires=install_requires
 )
