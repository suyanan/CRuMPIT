from pymongo import MongoClient
import sys

port=27017
ip='163.1.213.195'
client = MongoClient(ip, port)
db = client[sys.argv[1]]

def getCursor(coll,readID="$read_id"):
    cursor = coll.aggregate(
    [
        {"$group": {"_id": "$read_id", "unique_ids": {"$addToSet": "$_id"}, "count": {"$sum": 1}}},
        {"$match": {"count": { "$gte": 2 }}}
    ], allowDiskUse=True
    )
    return cursor

def removeDups(c,coll):
    response = []
    for doc in c:
        del doc["unique_ids"][0]
        for id in doc["unique_ids"]:
            response.append(id)

    batchsize=10000
    for i in range(0, len(response), batchsize):
        r=response[i:i+batchsize]
        coll.delete_many({"_id": {"$in": r}})

collections = [db.alba_summary,db.cent_stats]
readids=["$read_id","$read_id"]

for c,r in zip(collections,readids):
    print(c)
    cur=getCursor(c,readID=r)
    removeDups(cur,c)


