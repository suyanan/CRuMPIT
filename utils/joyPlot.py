import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set(style="white")
from pymongo import MongoClient
from bson.objectid import ObjectId
import sys
from argparse import ArgumentParser, SUPPRESS
from ete3 import NCBITaxa
ncbi = NCBITaxa()

ngons = ncbi.get_descendant_taxa(485, collapse_subspecies=False, intermediate_nodes=True)
ngons.append(485)

def test_data():
    # Create the data
    rs = np.random.RandomState(1979)
    x = rs.randn(500)
    g = np.tile(list("ABCDEFGHIJ"), 50)
    df = pd.DataFrame(dict(x=x, g=g))
    m = df.g.map(ord)
    df["x"] += m
    print(df)

def getRows(hce,b):
    n,l=0,[]
    for h in hce:
        l.append(h)
        if n>b:break
        n+=1
    return l
    

def getRun(r,ip='127.0.0.1',port=27017):
    client = MongoClient(ip,port)
    dbname=str(r)
    db = client[dbname]
    collection = db.cent_stats

    b=5000
    l=[]
    #bars=collection.distinct('barcode')
    bars=['none','BC01','BC02','BCO3','BC04','BC05','BC06','BC07','BC08','BC09','BC10','BC11','BC12']
    for bar in bars:
        hce = collection.find({"barcode": bar})
        i=getRows(hce,b)
        if len(i) >  0:
            l.extend(i)

    if len(l) == 0:
        print("no data from mongodb?")
        sys.exit()
    df =  pd.DataFrame(l)
    df['run']=r
    df['runBar']=r + '_' + df['barcode']
    df['prokaryote/Human'] = np.where(df['taxID']!=9606,'prokaryote','human')
    #df['Ngon'] = np.where(df['taxID'].isin(ngons),'Ngon','Not Ngon')
    return df

def getData(runs,ip='127.0.0.1',port=27017):
    dfs=[]
    for run in runs:
        dfs.append(getRun(run,ip=ip,port=port))
    df=pd.concat(dfs)
    df.to_csv('mixdata.csv')
    return df


# Define and use a simple function to label the plot in axes coordinates
def label(x, color, label):
    ax = plt.gca()
    ax.text(0, .2, label, fontweight="bold", color=color,
            ha="left", va="center", transform=ax.transAxes)

def plot(df):
    # Initialize the FacetGrid object
    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    g = sns.FacetGrid(df, row="runBar", hue="runBar", aspect=15, palette=pal)
    
    # Draw the densities in a few steps
    g.map(sns.kdeplot, "queryLength", clip_on=False, shade=True, alpha=1, lw=1.5, bw=.2)
    g.map(sns.kdeplot, "queryLength", clip_on=False, color="w", lw=2, bw=.2)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)
    g.map(label, "queryLength")
    
    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-.25)
    
    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)
    plt.show()

def plot2(df,collapse=False):
    if collapse==True:
        runBar='run'
    else:
        runBar='runBar'
    ax = sns.violinplot(x=runBar,
            y="queryLength",
            hue='prokaryote/Human',
            #hue='Ngon',
            #hue='prokaryote/Human',
            split=True,
            data=df)
    plt.ylim((0,10000))
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()

def stats(df):
    df2=pd.DataFrame()
    df2['means']=df.groupby(['run'])['queryLength'].mean()
    df2['medians']=df.groupby(['run'])['queryLength'].median()
    df2['maxes']=df.groupby(['run'])['queryLength'].max()
    df2['min']=df.groupby(['run'])['queryLength'].min()
    df2['stdv']=df.groupby(['run'])['queryLength'].std()
    print(df2)
    df2.to_csv('read_length_stats.csv')

def preMapArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True, nargs='+',
                             help='Specify sample name(s) as used in mongoDB.')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
    parser.add_argument('-c', '--collapse', required=False, action='store_true',
                             help='port address for mongoDB database')
    return parser

def runlengthPlot(opts):
    runs=opts.sample_name
    df=getData(runs,ip=opts.ip,port=opts.port)
    stats(df)
    plot2(df,collapse=opts.collapse)


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='read lengths from mongodb')
    parser = preMapArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run script

    runlengthPlot(opts)
