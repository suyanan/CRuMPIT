import sys
from pymongo import MongoClient
from argparse import ArgumentParser
import pandas as pd
from ete3 import NCBITaxa
ncbi = NCBITaxa()

def txname(taxids):
        taxid2name = ncbi.get_taxid_translator(taxids)
        return taxid2name


class runTable:
    def __init__(self,sample,ip,port,mapout,summaries='crumpit_summaries'):
        self.sample=sample
        self.ip=ip
        self.port=port
        self.client = MongoClient(self.ip, self.port)
        self.db=self.client[self.sample]
        self.mapout=mapout

    def getMaps(self):
        # get pointer from mongodb and make pandas df
        hce = self.db['map_depths']
        df = pd.DataFrame(list(hce.find()))
        # get list of taxids for taxid -> species name dict and add species name column
        taxids=list(df.taxid.unique())
        taxid2name=txname(taxids)
        df['taxid']=df.taxid.map(int)
        df['Species name']=df.taxid.map(taxid2name)
        # reduce to columns of interest and sort then save to csv
        df=df[['sample','barcode','Species name','chrom','len','covBread_1x_percent','avCov','x1','x5','x10']]
        df=df.sort_values(by=['sample','barcode','Species name'])
        df.to_csv(self.mapout,index=False)



def getTableArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True,
                         help='name of run or sample in mongodb')
    parser.add_argument('-t', '--table_name', required=False,default='crumpit_summaries',
                         help='crumpit_summaries is the name of the default db setup')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                         help='port address for mongoDB database')
    parser.add_argument('-o', '--map_out_csv', required=False, default='map.csv',
                         help='Output csv file for mapping stats')
    return parser


def runTables(opts):
    t=runTable(opts.sample_name,
            opts.ip,
            opts.port,
            opts.map_out_csv,
            summaries=opts.table_name)
    t.getMaps()

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='get details from crumpitdb make tables')
    parser = getTableArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    runTables(opts)
