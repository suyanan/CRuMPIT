import numpy as np
import pandas as pd
from Bio import SeqIO
import gzip
import sys
import re
from tqdm import tqdm
from argparse import ArgumentParser, SUPPRESS
import logging
logging.basicConfig(filename='example.log',level=logging.DEBUG)

baseINTRef={'A':1,
        'T':2,
        'C':3,
        'G':4,
        'N':5,
        '_':0}
INTbaseRef = {v: k for k, v in baseINTRef.items()}

baseINT={'A':1,
        'a':2,
        'T':3,
        't':4,
        'C':5,
        'c':6,
        'G':7,
        'g':8,
        'N':9,
        'n':10,
        '_':0}

INTbase = {v: k for k, v in baseINT.items()}

bases = ['A',
        'T',
        'C',
        'G',
        'a',
        't',
        'c',
        'g']

class predict:
    def __init__(self,mpileupfile,fasta,refs,depth,minRev,minFreq):
        self.mpileupfile = mpileupfile
        self.m = 200
        self.depth = depth
        self.minFreq=minFreq
        self.minRev=minRev
        self.outfast = fasta
        self.refs = refs
             
    def reBase(self,i):
        if np.max(i) < 0.15:
            return 'n'
        try:
            return INTbaseRef[np.argmax(i)] 
        except: return 'n'

    def refInfo(self):
        self.refStats,self.refDescript={},{}
        refFile=self.refs
        if refFile.endswith('gz'):
            op=gzip.open
        else:
            op=open
        with op(refFile,'rt') as inF:
        #with open(refFile,'rt') as inF:
            for seq in SeqIO.parse(inF,'fasta'):
                chrom=str(seq.id)#.replace('.','_')
                self.refStats[chrom] = { 'len' : len(seq.seq) ,
                        'covpos' : np.zeros(len(seq.seq) ),
                        'prediction' :  np.zeros((len(seq.seq), 6)),
                        'baspos' : np.chararray(len(seq.seq) ) }
                self.refStats[chrom]['baspos'][:] = 'N'
                self.refDescript[chrom] = seq.description


    def deBase(self,i):
        #if i in bases:
        b = baseINT[i]
        return b
    
    def strGap(self,seq,ind,gaps):
        ''' return string with gaps removed'''
        i0=0
        seq2=''
        for i,g in zip(ind,gaps):
            seq2+=seq[i0:i]
            i0 = i+g+2
        seq2+=seq[i0:]
        return seq2

    def seqBase(self,seq):
        '''return pileup column string of just bases and no indel strings'''
        if len(seq) == 1 and seq[0] not in bases:
            return 'N'
        indel = False
        indelN = 0
        base=[]

        #seq=re.sub('[*>$^]','',seq)
        #find all insertion positiosn
        ins=[m.start() for m in re.finditer('\+', seq)]
        # get length of gaps
        gaps = [int(seq[n+1])  if seq[n+1] not in bases else 0 for n in ins ]
        # create new seq without gaps
        seq2=self.strGap(seq,ins,gaps)

        #find all deletion positiosn
        dels=[m.start() for m in re.finditer('\-', seq2)]
        # get length of gaps
        gaps = [int(seq2[n+1])  if seq2[n+1] not in bases else 0 for n in dels ]
        seq3=self.strGap(seq2,dels,gaps)

        return seq3 

    def countBases(self,bases,cov):
        l=[]
        for i in INTbase:
            c=bases.count(i)
            if c != 0:
                l.append(bases.count(i))#/cov)
            else:l.append(0)
        l.append(cov)
        return l

    def loader(self,):
        '''load data from mpileup file and return generator including np arrary, pos and chrom'''
        m=self.m
        with open(self.mpileupfile,'rt') as f:
            n=sum(1 for line in f)

        with open(self.mpileupfile,'rt') as f:
            for line in tqdm(f, total=n):
                l=line.split('\t')
                b = self.seqBase(l[4])
                #if len(b) > m:
                #    b=b[:m]
    
                #bi = list(map(self.deBase,b))
                #bi = list(filter(None.__ne__, bi))
                bi = [ baseINT[i] for i in b if str(i) in bases]
                pos = int(l[1])
                chrom = str(l[0])
                cov=int(l[3])
                counts=self.countBases(bi,cov)
                yield bi,pos,chrom,counts

    def rowBase(self,r):
        b=[]
        b.append(r[0]+r[1])
        b.append(r[2]+r[3])
        b.append(r[4]+r[5])
        b.append(r[6]+r[7])
        return b


    def predict(self,a):
        '''return predictions from numpy array'''
        calls=[]
        for r in a:
            Dp = sum(r[:8])
            if Dp < self.depth: 
                calls.append(5)
                logging.info('below depth\t{0}'.format(r))
                continue
            b=np.array(self.rowBase(r))
            Br = b.max() # number of reads supporting most abundant base
            Bm = b.argmax() # the index of the base with the most reads
            Bp = (float(Br) / Dp)*100 # percentage of bases for top
            if Bp < self.minFreq: 
                calls.append(5)
                logging.info('below minFreq\t{0}'.format(r))
                continue
            if all([r[Bm*2] < self.minRev, r[(Bm*2)+1] < self.minRev]):
                calls.append(5)
                logging.info('below minRev\t{0}'.format(r))
                continue
            calls.append(Bm+1)
        return calls

    def predictCalls(self):
        mps = self.loader()
        block=100000
        cols = 0

        chroms={}
        chromPos={}
        print('loading data...')
        #clf = joblib.load(self.modelf) 

        for mp,pos,chrom,counts in mps:
            #p = self.model.predict( np.array( [mp] ) )
            chroms.setdefault(chrom,[]).append(counts)
            chromPos.setdefault(chrom,[]).append(pos)
            
            if cols < block:
                cols+=1
            else:
                cols = 0
                #print('Predicting chunk...')
                for chrom in chroms:
                    #predictions = clf.predict(np.array(chroms[chrom]))
                    predictions = self.predict(np.array(chroms[chrom]))
                    basePreds = map(INTbaseRef.get,predictions)
                    for pos,p in zip(chromPos[chrom],basePreds):
                        self.refStats[chrom]['baspos'][pos - 1] = p
                chroms={}
                chromPos={}

        print('Predicting final chunk...')
        for chrom in chroms:

            #predictions = clf.predict(np.array(chroms[chrom]))
            predictions = self.predict(np.array(chroms[chrom]))
            basePreds = map(INTbaseRef.get,predictions)
            for pos,p in zip(chromPos[chrom],basePreds):
                self.refStats[chrom]['baspos'][pos - 1] = p
                #self.refStats[chrom]['prediction'][pos - 1] = p

    def makeFasta(self):
        with open(self.outfast,'wt') as outf:
            s=''
            for chrom in self.refStats:
                s+='>{0}\n{1}\n'.format( chrom,
                        self.refStats[chrom]['baspos'].tostring().decode('utf-8') )
            outf.write(s)

    def savePreds(self):
        for chrom in self.refStats:
            np.save('{0}.npy'.format(chrom),self.refStats[chrom]['prediction'])

def runPredict(opts):
    pML = predict(opts.mpileup,opts.fasta,opts.refs,opts.depth,opts.minRev,opts.minFreq)
    print('Getting ref info')
    pML.refInfo()
    pML.predictCalls()
    print('making output files...')
    pML.makeFasta()
    #pML.savePreds()

def predictGetArgs(parser):
    parser.add_argument('-m', '--mpileup', required=True,
                                 help='mpileup file from mapped bam')
    parser.add_argument('-f','--fasta',required=False,default='cons.fasta',
                                             help='outfile fasta file name')
    parser.add_argument('-r','--refs',required=True,
                                             help='reference fasta file')
    parser.add_argument('-d','--depth',required=False,default=8,type=int,
                                             help='depth of reads needed, default=8')
    parser.add_argument('-R','--minRev',required=False,default=2,type=int,
                                             help='minimum supporting seqs in reverse orientation, default=2')
    parser.add_argument('-F','--minFreq',required=False,default=75,type=int,
                                             help='frequency (percent) of majority bases needed')
    return parser

if __name__ == '__main__':
    parser = ArgumentParser(description='consML predict. predict consesus/variants from mpileup using trained NN')
    parser = predictGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    # run
    runPredict(opts)
