#!/usr/bin/env python3
import sys
import pandas as pd
from argparse import ArgumentParser


def getMeta(mp):
    genes=['penA','mtrR', 'ponA', 'parC', 'porB','gyrA', '23S']
    dfs=[]
    for gene in genes:
        f='{0}/{1}_metadata.txt'.format(mp,gene)
        df=pd.read_csv(f,sep='\t')
        df['gene']=gene
        df['geneAlle']=df['gene']+df['Allele Type'].map(str)
        dfs.append(df)
    df=pd.concat(dfs)
    return df

def addGene(s):
    genes=['penA','mtrR', 'ponA', 'parC', 'porB','gyrA', '23S']
    for gene in genes:
        if s.startswith(gene):
            return gene

def roundup(x,n=50):
    return int(n * round(float(x)/n))


def runpb6(opts):
    headers=['qseqid', 'sseqid', 'pident', 'length', 'mismatch',\
            'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', 'bitscore']
    df=pd.read_csv(opts.blast_file,names=headers,sep='\t')
    # get just gene name from allele
    df['gene']=df.sseqid.map(addGene)
    # round up to closest 50 so slight variations in blast hit are not counted as seperate loci
    df['fqstart']=df.qstart.map(roundup)
    # get top hits from bitscore (not sure what is best)
    idx=df.groupby(['gene','fqstart'])['bitscore'].transform(max) == df['bitscore']
    topHits=df[idx]

    # get meta data and add to top hits
    mdf = getMeta(opts.meta_files)
    df=topHits.merge(mdf, how='left',left_on='sseqid',right_on='geneAlle')
    df.drop_duplicates(subset=['fqstart'],inplace=True)
    df.to_csv(opts.output_file)




def pb6Args(parser):
    parser.add_argument('-b', '--blast_file', required=True, 
                             help='Specify blast6 file')
    parser.add_argument('-m', '--meta_files', required=True,
                             help='Specify ngstar directory containing allele meta data files')
    parser.add_argument('-o', '--output_file', required=True,
                             help='Specify output csv file')

    return parser


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='parse blastn format 6 output of ngstart alleles from Ngon genome')
    parser = pb6Args(parser)
    opts, unknown_args = parser.parse_known_args()
    # run script

    runpb6(opts)
