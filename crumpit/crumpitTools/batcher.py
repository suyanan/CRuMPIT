#!/usr/bin/env python3
import sys
import subprocess
from threading import Thread
import shutil
from argparse import ArgumentParser, SUPPRESS
import time
import os

class batcher:
    def __init__(self,path,outfol,batchsize=1000,batches=0,fileType='fast5',gridFiles=8000,porechop='off'):
        self.job_queue=[]
        self.fileType=fileType
        self.batchsize=int(batchsize)
        self.gridFiles=int(gridFiles)
        self.batches=batches
        self.path=path + '/'
        self.outpath=outfol
        self.stop=False
        self.previousf5s=set()
        self.runNums={}
        self.reruns=0
        self.porechop=porechop

    def getPreviousBatches(self):
        out = subprocess.check_output(["find", "{0}".format(self.outpath),"-name","*.batch*"],universal_newlines=True)
        for bf in out.splitlines()[:-1]:
            self.batches+=1
            with open(bf,'rt') as handle:
                for f in handle.read().split('\n'):
                    self.previousf5s.add(f)
            shutil.move(bf, '{0}_previous'.format(bf))

    def getFiles(self,path):
        #if self.fileType=='fastq':
        #    maxdepth='4'
        #else: maxdepth='10'
        maxdepth='10'
        try:
            out=subprocess.check_output(["find", "{0}".format(path),'-maxdepth',maxdepth,"-name","*.{0}".format(self.fileType)],universal_newlines=True)
            out=out.splitlines()
            #if len(out)>1: del out[-1]
        except subprocess.CalledProcessError as e:
            #print(e.output,file=sys.stderr)
            #print("Find failed...",file=sys.stderr)
            out=[]
        return out

    def run_batch(self,b):
        if self.fileType=='fast5':
            outfile="{0}/{1}.batch".format(self.outpath,self.batches)
        else:
            outfile="{0}/{1}.batch".format(self.outpath,b[0].split(',')[1])

#        outfile="{0}/{1}.batch".format(self.outpath,b[0].split(',')[1])
        with open(outfile,'wt') as outf:
            for i in b: outf.write("{0}\n".format(i))

    def run(self):
        self.getPreviousBatches()
        self.f5s=self.getFiles(self.path)
        self.npf5s=set(self.f5s)-self.previousf5s
        for f in self.npf5s:
            if len(self.job_queue) >= self.batchsize:
                self.run_batch(self.job_queue)
                self.batches+=1
                self.job_queue=[f]
            else:
                self.job_queue.append(f)
                self.f5s.extend(f)
        self.f5s.extend(self.previousf5s)
        del(self.previousf5s)
        if len(self.job_queue)==0:return
        else:
            self.batches+=1
            self.run_batch(self.job_queue)

    def keepWatch(self):
        while self.stop is False:
            newF5s=self.getFiles(self.path)
            newF5s=set(newF5s)-set(self.f5s)
            for f in newF5s:
                if len(self.job_queue) >= self.batchsize:
                    self.run_batch(self.job_queue)
                    self.batches+=1
                    self.job_queue=[]
                else:
                    self.job_queue.append(f)
                    self.f5s.append(f)

#    def gridFin(self,fq):
#        '''check if number of fast5 files == batch'''
#        base=os.path.basename(fq)
#        path=os.path.dirname(fq)
#        num=int(base.replace("fastq_","").replace(".fastq",""))
#        readsFol="{0}/reads/{1}".format(path,num)
#        try:
#            f5s=os.listdir(readsFol)
#        except:
#            return False
#        filReads = filter(lambda x: x.endswith('.fast5'), f5s)
#        if len(list(filReads)) >= self.gridFiles: return True
#        else: return False

    def gridFin(self,fq):
        '''check if fast5 file also available'''
        base=os.path.basename(fq)
        #num=int(base.split('_')[-1].replace('.fastq',''))
        seq=base.replace('.fastq','')
        path=os.path.dirname(fq)
        path=path.replace('fastq_pass','fast5_pass')
        f5='{0}/{1}.fast5'.format(path,seq)
        if os.path.isfile(f5): return True
        else: return False

    def gridNameSplit(self,f):
        '''provide vals and files/fols from gridion for nextflow'''
        path=os.path.dirname(f)
        barcode=path.split('/')[-1]
        if barcode=='fastq_pass':
            barcode=''
            runNum=path.split('/')[-2]
        else:
            runNum=path.split('/')[-3]

        pathBase=path.replace('fastq_pass','').replace('fastq_fail','')
        base=os.path.basename(f)
        num=int(base.split('_')[-1].replace('.fastq',''))
        if self.porechop == 'off':
            lane=pathBase.split('/')[-3].split('_')[2]
            #barcode=pathBase.split('/')[-1]
            if barcode!='':
                pathBase=pathBase.replace('/{0}'.format(barcode),'')
            seq=base.replace('.fastq','')
            f5='{0}/fast5_pass/{1}/{2}.fast5'.format(pathBase,barcode,seq)
        else:
            lane=pathBase.split('/')[-2].split('_')[2]
            seq=base.replace('.fastq','')
            f5='{0}/fast5_pass/{1}.fast5'.format(pathBase,seq)
        #    runNum=pathBase.replace(self.path,'').split('/')[1]
#        runNum=pathBase.replace(self.path,'').split('/')[1]
        try:
            seqsums=os.listdir("{0}/sequencing_summary/".format(pathBase))
            sumfile="{0}/sequencing_summary/{1}".format(pathBase,seqsums[0])
        except:
            sumfile=None
        #runNum=pathBase.replace(self.path,'').split('/')[1]
        if runNum not in self.runNums:
            self.runNums[runNum]=self.reruns
            self.reruns+=1
        if self.porechop == 'off':
            return "{0},r{5}{6}b{1},{2},{3},{4},{6}".format(lane,num,f,f5,sumfile,self.runNums[runNum],barcode)
        else:
            return "{0},r{5}b{1},{2},{3},{4}".format(lane,num,f,f5,sumfile,self.runNums[runNum])


#    def gridNameSplit(self,f):
#        '''provide vals and files/fols from gridion for nextflow'''
#        path=os.path.dirname(f)
#        base=os.path.basename(f)
#        num=int(base.replace("fastq_","").replace(".fastq",""))
#        lane=path.split('/')[-1]
#        reads="{0}/reads/{1}".format(path,num)
#        sumfile="{0}/sequencing_summary_{1}.txt".format(path,num)
#        return "{0},{1},{2},{3},{4}".format(lane,num,f,reads,sumfile)

    def runGrid(self):
        self.getPreviousBatches()
        self.f5s=self.getFiles(self.path)
        self.npf5s=set(self.f5s)-self.previousf5s
        unfinished=[]
        for f in self.npf5s:
            if self.gridFin(f) == False:
                self.f5s.remove(f)
                continue
            ft=self.gridNameSplit(f)
            self.job_queue.append(ft)
            self.run_batch(self.job_queue)
            self.batches+=1
            self.job_queue=[]
            self.f5s.extend(f)

        self.f5s.extend(self.previousf5s)
        del(self.previousf5s)
        if len(self.job_queue)==0:return
        else:
            self.batches+=1
            self.run_batch(self.job_queue)

    def keepWatchGrid(self):
        while self.stop is False:
            newF5s=self.getFiles(self.path)
            newF5s=set(newF5s)-set(self.f5s)
            for f in newF5s:
                if self.gridFin(f) == False: continue
                ft=self.gridNameSplit(f)
                self.job_queue.append(ft)
                self.f5s.append(f)
                self.run_batch(self.job_queue)
                self.batches+=1
                self.job_queue=[]
            time.sleep(60)



    def runWatch(self,grid=False):
        if grid==False:
            rcv=Thread(target=self.keepWatch)
        else:
            rcv=Thread(target=self.keepWatchGrid)
        rcv.start()

    def stopWatch(self):
        self.stop=True


def bctRunAll(opts):
    if opts.grid==True:
        bctRunGrid(opts)
    else: bctRun(opts)

def makeFol(fol):
    if not os.path.exists(fol):
        os.makedirs(fol)

def bctRun(opts):
    makeFol(opts.outfold)
    bt=batcher(opts.path,opts.outfold,batchsize=opts.batchsize,batches=opts.batches)
    bt.run()
    if opts.watch==True:
        try:
            bt.runWatch()
        except KeyboardInterrupt:
            bt.stopWatch()

def bctRunGrid(opts):
    makeFol(opts.outfold)
    bt=batcher(opts.path,opts.outfold,batchsize=1,batches=opts.batches,fileType='fastq',gridFiles=opts.gridFiles)
    bt.runGrid()
    batches=0
    if opts.watch==True:
        try:
            bt.runWatch(grid=True)
        #while True:
        #    try:
                #if bt.batches!= batches: print(bt.batches)
        #        batches=bt.batches
        except KeyboardInterrupt:
            bt.stopWatch()
            #sys.exit()

def bctGetArgs(parser):
    parser.add_argument('-p', '--path', required=True,
                                 help='Path to point at')
    parser.add_argument('-o', '--outfold', required=True,
                                 help='output path for batch files')
    parser.add_argument('-bs', '--batchsize', required=False,default=8000,type=int,
                                 help='batch size for basecalling (number of fast5 files)')
    parser.add_argument('-gf', '--gridFiles', required=False,default=8000,type=int,
                                 help='GridION fast5 batch size')
    parser.add_argument('-bn', '--batches', required=False,default=0,type=int,
                                 help='number of previous batch files if restarting')
    parser.add_argument('-g', '--grid', required=False,action='store_true',
                                 help='Option for GridION output')
    parser.add_argument('-w', '--watch', required=False,action='store_true',
                                 help='Option to watch output')
    return parser

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='CRuMPIT batcher: batch fast5 files or fastq file for CRuMPIT workflow')
    parser=bctGetArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    bctRunAll(opts)
