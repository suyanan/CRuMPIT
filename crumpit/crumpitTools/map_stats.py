# coding: utf-8
#get_ipython().magic('matplotlib notebook')
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
from matplotlib.ticker import FuncFormatter
from pymongo import MongoClient
from bson.objectid import ObjectId
import sys
from ete3 import NCBITaxa
ncbi = NCBITaxa()
import datetime
from dateutil.parser import parse
import operator
from cigar import Cigar
import warnings
warnings.filterwarnings("ignore")

class mpStats:
    def __init__(self,dbname,thresh=150,mdbip='127.0.0.1',mdbport=27017,score=1,bases=1,barcodes=None,last=False):
        client = MongoClient(mdbip, mdbport)
        self.dbname=str(dbname)
        self.db = client[dbname]
        self.score=score
        self.reads=bases
        self.thresh=thresh
        self.barcodes=barcodes
        self.last=last
        self.species={}

    def getSpecies(tax):
        lins=ncbi.get_lineage(tax)
        for l in lins:
            rank=ncbi.get_rank([l])
            if rank[l]=='species':
                return rank

    def allStats(self,barcode=False):
        collection = self.db.map_stats
        if self.last != False:
            if barcode==False:
                hce = collection.find({"_id": {"$gt": self.last}})
            else:
                hce = collection.find({"_id": {"$gt": self.last},"barcode":self.barcodes})
        else:
            if barcode==False:
                hce = collection.find()
            else:
                hce = collection.find({"barcode":self.barcodes})
        return hce


    def getMapStats(self,taxid=1):
        #a=mpStats(sample)
        taxes = ncbi.get_descendant_taxa(taxid, intermediate_nodes=True)
        taxes.append(int(taxid))

        if self.barcodes is None:hce = self.allStats()
        elif self.barcodes=='nobarcode':hce = self.allStats()
        else: hce = self.allStats(barcode=True)
        #hce = collection.find()
        #species=[]
        #d={}
        if taxid not in self.species:
            self.species[taxid]={}
        for h in hce:
            #print(h)
            if int(h['mapq']) < self.score: continue
            #if int(h['score']) < self.thresh: continue
            if int(h['TAXID']) not in set(taxes): continue
            p=parse(h['start_time'])
            self.species[taxid].setdefault(p,[]).append(h['CIGAR'])
            self.last=ObjectId(h['_id'])
        return self.species[taxid]

    def getCentStats(self,tax,until=60*60*24*2):
        kingdom,name=taxIDstats(tax)
        collection = self.db.cent_stats
        #hce = collection.find({"score": {"$gt": self.thresh-1},"taxID": {"$in": kingdom}})
        if self.barcodes is None: hce = collection.find({"taxID": {"$in": kingdom}})
        elif self.barcodes[0]=='nobarcode':hce = collection.find({"taxID": {"$in": kingdom}})
        else: hce = collection.find({"taxID": {"$in": kingdom},"barcode":self.barcodes})
        cbt={}
        # centrifuge bacterial bases over time
        for h in hce:
            if int(h['score']) >= self.thresh:
                cbt.setdefault(parse(h['start_time']),[]).append(h['queryLength'])
            else:
                cbt.setdefault(parse(h['start_time']),[]).append(0)
        sorted_x = sorted(cbt.items(), key=operator.itemgetter(0))
        b,r=0,0
        x,y=[],[]
        #start=sorted_x[0][0].timestamp()
        #stop=start+int(until)
        cbt_sum={}
        for i in sorted_x:
            #if i[0].timestamp() > stop: break
            p=i[0]
            b+=int(sum(i[1]))
            r+=1
            x.append(p)
            y.append(b)
            s=i[0].timestamp()
            cbt_sum.setdefault(int(s),b)
        return x,y,cbt_sum

    def getData(self,ti,until=60*60*24*2,ratiochart=False):
        d=self.getMapStats(taxid=ti)
        sorted_x = sorted(d.items(), key=operator.itemgetter(0))
        b,r=0,0
        x,y=[],[]
        #start=sorted_x[0][0].timestamp()
        #stop=start+int(until)
        for i in sorted_x:
            #if i[0].timestamp() > stop: continue
            p=i[0]
            b+=int(len(Cigar(i[1][0])))
            r+=1
            x.append(p)
            if ratiochart == True:
                s=i[0].timestamp()
                y1=(float(b)/self.bbases[int(s)])*100
            else:
                y1=b
            y.append(y1)
        return x,y

    def visTimings(self,taxes,showChart='n',ut=60*60*24*2):
       txname = ncbi.get_taxid_translator(taxes)
       if showChart=='y': fig,ax=plt.subplots()
       x,y=[],[]
       rDict={}
       # mapping for each taxid
       for t in taxes:
           x1,y1=self.getData(str(t),until=ut)
           if len(y1) < 1: continue
           if max(y1) < self.reads: continue
           x.append(x1)
           y.append(y1)
           rDict[str(t)]={'bases':max(y1),'Reads':len(y1)} # diction of basic stats to return
           if showChart=='y': ax.plot_date(x1,y1,label="{0}".format(txname[int(t)]))
       if len(y) == 0: return
       #plt.ylim(0,y[-1])
       #plt.xlim(x[0],x[-1])
       if showChart=='y':
            ax.xaxis.set_major_formatter( DateFormatter('%H:%M') )
            ax.get_yaxis().set_major_formatter( FuncFormatter(lambda x, p: format(int(x), ',')))
            plt.xlabel('Time of day',fontsize=18)
            plt.ylabel('Bases',fontsize=18)
            plt.legend(loc='upper left')
             #plt.show()
            plt.savefig("{0}_mapbases.pdf".format(self.dbname),dpi=600)
       return rDict


    def visRatioTimings(self,taxes,showChart='n',ut=60*60*24*2):
       txname = ncbi.get_taxid_translator(taxes)
       fig,ax=plt.subplots()
       x,y=[],[]
       rDict={}
       # centrifuge stats
       cx1,cy1,self.bbases=self.getCentStats(2,until=ut)
       #ax.plot_date(cx1,cy1,label="Total Bacterial Cenrtifuge")

       # mapping for each taxid
       for t in taxes:
           x1,y1=self.getData(t,until=ut,ratiochart=True)
           if len(y1) < 1: continue
           if max(y1) < self.reads: continue
           #x.append(x1)
           y.append(y1)
           ax.plot_date(x1,y1,label="{0}".format(txname[int(t)]))
           ax.set_ylim(ymin=0,ymax=100)
       if len(y) == 0: return
       ax.xaxis.set_major_formatter( DateFormatter('%H:%M') )
       plt.ylim([0,100])
       #ax.get_yaxis().set_major_formatter( FuncFormatter(lambda x, p: format(int(x), ',')))
       plt.xlabel('Time of day',fontsize=18)
       plt.ylabel('Percent Bases',fontsize=18)
       plt.legend(loc='upper left')
       if showChart=='y':
            #plt.show()
            plt.savefig("{0}_ratio.pdf".format(self.dbname),dpi=600)

if __name__=="__main__":
    visTimings(sys.argv[1],sys.argv[2:])
