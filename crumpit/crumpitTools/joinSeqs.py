#!/usr/bin/env python
import sys
from Bio import SeqIO
import gzip
#from itertools import izip

def yieldFq(fq):
    for seq in SeqIO.parse(gzip.open(fq,'rt'),'fastq'):
        #print seq.letter_annotations
        yield seq



def phredTo33String(it):
    l=it.letter_annotations['phred_quality']
    s=[chr(i+33) for i in l]
    return "".join(s)

if __name__ == '__main__':
    f1=yieldFq(sys.argv[1])
    f2=yieldFq(sys.argv[2])
    for fq1,fq2 in zip(f1,f2):
        print("@{0}\n{1}N{2}\n+\n{3}N{4}".format(fq1.id,fq1.seq,fq2.seq,phredTo33String(fq1),phredTo33String(fq2)))
