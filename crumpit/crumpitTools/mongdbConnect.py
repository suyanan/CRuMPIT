#!/usr/bin/env python3
from pymongo import MongoClient

class mdbConnect:
    '''Class for connecting to MongoDB database, can be ingerited '''
    def __init__(self,**kwargs):
        allowed=['table_name','ip','port']
        for k, v in kwargs.items():
            if k in allowed: setattr(self, k, v)
        client = MongoClient(self.ip, self.port)
        self.db = client[self.table_name]
        self.collection = self.db.nanopore_runs
