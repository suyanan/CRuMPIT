#!/usr/bin/env python
import sys
import sqlite3

def createTable(c):
    # Create table
    c.execute('''CREATE TABLE if not exists albacore_stats
                         (filename,read_id primary key,run_id, channel, start_time, duration, num_events, template_start, num_events_template, template_duration, num_called_template, sequence_length_template, mean_qscore_template, strand_score_template)''')

def insertData(many,c):
    # Insert a row of data
    c.executemany("INSERT OR REPLACE INTO albacore_stats VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",many)

def getData(f):
    #l=[i.split(',')[0:1] for i in open(f[0],'r').read().split('\n')]
    l=[]
    for line in open(f,'r'):
        line=line.replace('\n','')
        li=line.split('\t')
        if str(li[0])=="filename": continue
        l.append([str(li[0]),str(li[1]),str(li[2]),int(li[3]),float(li[4]),float(li[5]),float(li[6]),float(li[7]),float(li[8]),float(li[9]),float(li[10]),float(li[11]),float(li[12]),float(li[13])])
    return tuple(l)

def dropTab(c):
    c.execute('''drop table if exists albacore_stats''')

def run():
    conn = sqlite3.connect(sys.argv[2])
    c = conn.cursor()
    #dropTab(c)
    createTable(c)
    t=getData(sys.argv[1])
    insertData(t,c)
    # Save (commit) the changes
    conn.commit()
    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()

if __name__ == '__main__':
    run()
