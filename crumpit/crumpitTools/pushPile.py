#!/usr/bin/env python3
import sys
import json
import os
import subprocess
from Bio import SeqIO
import gzip
import pymongo
from pymongo import UpdateOne
from pymongo import MongoClient
import pysam
from cigar import Cigar
from argparse import ArgumentParser, SUPPRESS
import os.path
from tqdm import tqdm
import json
import numpy as np
from bokeh.plotting import figure, output_file, save
from bokeh.layouts import column
from bokeh.models import HoverTool
import pandas as pd

def rolling_window(a,window):
    df = pd.DataFrame(a)
    df['avg'] = df.rolling(window).mean()
    y = df['avg'].values.tolist()[0::window]
    x = [i*window for i in range(len(y))]
    return x,y

class pullPile:
    def __init__(self,sample,barcode,base,ip='127.0.0.1',port=27017,batchNum=0,pf=None,rf=None,outHtml=None):
        self.refDict=self.load_obj(base + '/refs.json')
        self.pileFile=pf
        self.rf=rf
        self.outHtml=outHtml
        self.basedir=base + '/'
        client = MongoClient(ip, port)
        self.name = sample
        self.db = client[self.name]
        self.collection = self.db.mpileup
        self.barcode=barcode
        self.getInfo()
        self.plots = []

    def getMpileup(self):
        '''return dictionary of each mpileup file'''
        posts=[]
        with open(self.pileFile,'rt') as pileUp:
            for line in pileUp:
                l=line.split('\t')
                if len(l) == 6:
                    p={ 'pos':int(l[1]),
                        'ref':str(l[2]),
                        'cov':int(l[3]),
                        'seq':list(str(l[4])),
                        'qual':list(str(l[5]).replace('\n','')),
                        'chrom':str(l[0]),
                        'barcode':self.barcode }
                elif len(l) == 5:
                    p={ 'pos':int(l[1]),
                        'ref':str(l[2]),
                        'cov':int(l[3]),
                        'seq':list(str(l[4]).replace('\n','')),
                        'chrom':str(l[0]),
                        'barcode':self.barcode }
                else: continue
                #posts.append(p)
                yield p

        #return posts

    def whichRef(self,taxid):
        '''return reference file from json (dictionary) given taxid'''
        try:
            return self.refDict[taxid]
        except:
            # hack for tb
            try:
                if str(taxid)=='1763':
                    return self.refDict["83332"]
            except: return "no reference"
            return "no reference"

    def load_obj(self, name ):
        with open( name , 'r') as f:
            return json.load(f)

    def getInfo(self):
        self.chroms=self.collection.distinct('chrom')
        self.barcodes=self.collection.distinct('barcode')

    def getAllPos(self):
        hce=self.collection.find()
        return hce

    def getSomePos(self,barcode,chrom):
        hce=self.collection.find({'chrom':chrom,'barcode':barcode})
        return hce

    def taxToRef(self,tax):
        if self.rf==None:
            self.ref=self.whichRef(tax)
        else:
            self.ref=self.rf
        self.taxid = tax

    def refInfo(self):
        self.refStats,self.refDescript={},{}
        if self.rf==None:
            refFile='{0}/{1}'.format(self.basedir,self.ref)
        else:
            refFile=self.rf
        with gzip.open(refFile,'rt') as inF:
            for seq in SeqIO.parse(inF,'fasta'):
                chrom=str(seq.id).replace('.','_')
                self.refStats[chrom] = { 'len' : len(seq.seq) , 'covpos' : np.zeros(len(seq.seq)+1 ) }
                self.refDescript[chrom] = seq.description

    def calcCover(self,hce,chrom):
        for h in hce:
            chrom=h['chrom'].replace('.','_')
            self.refStats[chrom]['covpos'][h['pos']]=h['cov'] 

    def prepPlots(self):
        outD = 'plots/{0}/{1}'.format(self.barcode,self.taxid)
        if not os.path.exists(outD):
            os.makedirs(outD)
        if self.outHtml!=None:
            output_file(self.outHtml)
        else:
            output_file('{0}/{1}.html'.format(outD,self.taxid))

    def plotCov(self,covpos,chrom):
        # data
        pos,coverage = rolling_window(covpos, 100)
        data = dict(pos=pos,
                coverage=coverage)

        # figure
        p = figure(title="Chrom: {0}:{3},Barcode {1}, Run: {2}".format(chrom,
            self.barcode,
            self.name,
            self.refDescript[chrom]),
                x_axis_label='position on chromosome', 
                y_axis_label='Read coverage',
                plot_width=1200, plot_height=800)

        # draw figure
        p.line('pos', 'coverage', legend=chrom, line_width=2, source=data)

        # add hovertool 
        p.add_tools( HoverTool(tooltips = [
                ( 'pos',   '@pos{0,0}' ),
                ( 'coverage', '@coverage{0,0.0}' ),
            ],
            mode='vline' ))

        self.plots.append( p )

    def getCoverage(self):
        if self.pileFile!=None:
            hce=self.getMpileup()
            chrom=None
            self.calcCover(hce, chrom)
        for chrom in self.refStats:
            if self.pileFile==None:
                hce=self.getSomePos(self.barcode,chrom)
                self.calcCover(hce,chrom)
            self.refStats[chrom]['x1']=len(np.where( self.refStats[chrom]['covpos'] > 0 )[0])
            self.refStats[chrom]['x5']=len(np.where( self.refStats[chrom]['covpos'] > 4 )[0])
            self.refStats[chrom]['x10']=len(np.where( self.refStats[chrom]['covpos'] > 10 )[0])
            if self.refStats[chrom]['x1'] > 0:
                self.refStats[chrom]['covBread_1x_percent']=float(self.refStats[chrom]['x1']/self.refStats[chrom]['len'])*100
                self.refStats[chrom]['avCov']=np.sum(self.refStats[chrom]['covpos'])/self.refStats[chrom]['x1']
                self.refStats[chrom]['cov_stdv']=np.std(self.refStats[chrom]['covpos'])
            else:
                self.refStats[chrom]['covBread_1x_percent']=0
                self.refStats[chrom]['avCov']=0
                self.refStats[chrom]['cov_stdv']=0
            self.plotCov(self.refStats[chrom]['covpos'],chrom)
            #self.refStats[chrom]['covpos']=list(self.refStats[chrom]['covpos'])
            del self.refStats[chrom]['covpos']


def runPullPile(tax,sample,barcode,base,ip='127.0.0.1',port=27017,pf=None,rf=None,outHtml=None):
    pullP=pullPile(sample,barcode,base,ip=ip,port=port,pf=pf,rf=rf,outHtml=outHtml)
    pullP.taxToRef(str(tax))
    pullP.refInfo()
    pullP.prepPlots()
    pullP.getCoverage()
    save(column(pullP.plots))
    return pullP.refStats

def pullPileArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True,
                             help='Specify sample name as used in mongoDB.')
    parser.add_argument('-df', '--depth_file',required=True,
                             help='depth file')
    parser.add_argument('-b', '--barcode',required=False,default='none',
                             help='barcode name for multiplexed sample, default = none')
    parser.add_argument('-t', '--tax',required=True,
                             help='taxid')
    parser.add_argument('-r', '--refbase',required=True,
                             help='refbase folder')

    #optionals
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
    parser.add_argument('-rf', '--ref',required=False,default=None,
                             help='specific reference')
    parser.add_argument('-o', '--out_stats',required=False,default='out.json',
                             help='output json')
    parser.add_argument('-oh', '--outHtml',required=False,action='store_true',
                             help='output html')
    return parser


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='get info from depth file')
    parser = pullPileArgs(parser)


    opts, unknown_args = parser.parse_known_args()
    # run script
    stats=runPullPile(opts.tax,
            opts.sample_name,
            opts.barcode,
            opts.refbase,
            ip=opts.ip,
            port=opts.port,
            pf=opts.depth_file,
            rf=opts.ref,
            outHtml=opts.outHtml)
    with open(opts.out_stats, 'w') as outfile:
        json.dump(stats, outfile)
