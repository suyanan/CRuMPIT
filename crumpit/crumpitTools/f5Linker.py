#!/usr/bin/env python
import sys
import os
from argparse import ArgumentParser


def lnFile(f,outfol):
	d=f.split('/')[-1]
	os.symlink(f,'{1}/{0}'.format(d,outfol))

def processFile(f,outfol):
	for fi in open(f,'rt'):
		lnFile(fi.replace('\n',''),outfol)

def f5LArgs(parser):
    parser.add_argument('-i', '--infile', required=True,
                         help='input text file of list of fast5 sequences')
    parser.add_argument('-o', '--outfol', required=True,
                         help='output folder name')
    return parser



def f5LRun(opts):
    processFile(opts.infile,opts.outfol)


if __name__ == '__main__':
    parser = ArgumentParser(description='Symlink fast5 files from list of files')
    parser = f5LArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    f5LRun(opts)

