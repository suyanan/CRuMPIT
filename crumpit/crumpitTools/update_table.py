import pandas as pd
from pymongo import MongoClient


client = MongoClient('127.0.0.1', 27017)
db = client['gridRuns']
collection = db.gridRuns
hce=collection.find()
log={}
for h in hce: log[h['run_name']]=h

df=pd.DataFrame(log)
df=df.transpose()
df=df.sort_values(by=['starttime'],ascending=False)
df.to_html('/mnt/nanostore/gridRunInfo.html')

