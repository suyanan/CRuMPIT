#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
import gzip
import glob
from Bio import SeqIO
import os


class processFQs:
    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        #self.batchNum=int(self.flist.split('/')[-1].replace('.batch',''))
        self.barcodes={}
        self.parseNonHuman()
        self.processFiles()

    def makeFol(self,fol):
        if not os.path.exists(fol):
            os.makedirs(fol)

    def parseNonHuman(self):
        self.humanList=[]
        for line in gzip.open(self.cent,'rt'):
            l=line.split('\t')
            if str(l[2]) == '9606':
                self.humanList.append(str(l[0]))

    def clearFile(self,fq):
        #seqs=[]
        for seq in SeqIO.parse(open(fq,'rt'),'fastq'):
            if seq.id not in self.humanList:
                if 'barocde=' in seq.description:
                    barcode=seq.description.split(' ')[-1]
                    barcode=barcode.replace('barcode=','')
                else:
                    barcode='nobarcode'
                self.barcodes.setdefault(barcode,[]).append(seq)
#                seqs.append(seq)
        for bar in self.barcodes:
            fn=fq.split('/')[-1]
            outf='barcodes/{1}_{2}'.format(self.fol,bar,fn)
            self.makeFol('barcodes')
            with open(outf,'wt') as outhandle:
                SeqIO.write(self.barcodes[bar],outhandle,"fastq")

    def processFiles(self):
        fqs = glob.iglob('{0}/**/*.fastq'.format(self.fol), recursive=True)
        for fq in fqs:
            self.clearFile(fq)

def prFqs_args(parser):

    parser.add_argument('-fol', '--fol', required=True,
                        help='albacore folder')
    parser.add_argument('-c', '--cent', required=True,
                        help='centrifuge file')
    #parser.add_argument('-l', '--flist', required=True,
    #                    help='list of fast5 files')
    return parser


def prFqs_run(opts):
    kditc=vars(opts)
    processFQs(**kditc)

if __name__ == '__main__':
    parser = ArgumentParser(description='back up non-human fast5 files to location')
    parser = prFqs_args(parser)
    opts, unknown_args = parser.parse_known_args()
    prFqs_run(opts)
