import sys
from pymongo import MongoClient
from argparse import ArgumentParser
from bson.objectid import ObjectId
from crumpitTools.mongdbConnect import mdbConnect
import pprint
from ete3 import NCBITaxa
ncbi = NCBITaxa()

def txname(taxid):
        taxid2name = ncbi.get_taxid_translator([taxid])
        return taxid2name[taxid]

class getRuns(mdbConnect):
    def __init__(self,**kwargs):
        allowed=['samples','guuids']
        mdbConnect.__init__(self,**kwargs)
        for k, v in kwargs.items():
            if k in allowed: setattr(self, k, v)

    def allStats(self):
        hce = self.collection.find()
        return hce

    def printRuns(self):
        for h in self.allStats(): print(h)

    def someStats(self):
        hce = self.collection.find({"sample_ID":{"$in":self.samples}})
        return hce

    def getGuuids(self):
        hce = self.collection.find({"_id":{"$in":self.guuids}})
        return hce

def printr(h,domain):
    for i in h:
        for b in i['barcodes']:
            for f in i['barcodes'][b][domain]['spBases']:
                print("{0},{1},{2},{3},{4}".format(
                    i['sample_ID'],
                    b,
                    txname(int(f)),
                    i['barcodes'][b][domain]['spBases'][f]['bases'],
                    i['barcodes'][b][domain]['spBases'][f]['reads']))

def printm(h,domain):
    for i in h:
        for b in i['barcodes']:
            if i['barcodes'][b][domain]['minimap2'] == None: continue
            for f in i['barcodes'][b][domain]['minimap2']:
                if 'chroms' not in i['barcodes'][b][domain]['minimap2'][f]: continue
                for chrom in  i['barcodes'][b][domain]['minimap2'][f]['chroms']:
                    print("{0},{1},{2},{3},{4},{5},{6}".format(
                    i['sample_ID'],
                    b,
                    txname(int(f)),
                    chrom,
                    i['barcodes'][b][domain]['minimap2'][f]['chroms'][chrom]['covBread_1x_percent'],
                    i['barcodes'][b][domain]['minimap2'][f]['chroms'][chrom]['avCov'],
                    i['barcodes'][b][domain]['minimap2'][f]['chroms'][chrom]['cov_stdv']))


def run(kdict):
    if kdict['samples'] != None:
        kdict['samples']=kdict['samples'].split(',')
        m=getRuns(**kdict)
        h=m.someStats()
        
        if kdict['species'] == True:
            print("{0},{1},{2},{3},{4}".format(
         		'Run_name',
         		'Barcode',
         		'Taxid',
         		'Bases',
         		'Reads'))
            h=m.someStats()
            for i in h:
                for b in i['barcodes']:
                    for f in i['barcodes'][b]['Bacteria']['filtspecies']:
     	               print("{0},{1},{2},{3},{4}".format(
     					i['sample_ID'],
                                        	b,
     					txname(int(f)),
     			i['barcodes'][b]['Bacteria']['filtspecies'][f]['bases'],
     			i['barcodes'][b]['Bacteria']['filtspecies'][f]['reads']))    
            h=m.someStats()
            for i in h:
                for b in i['barcodes']:
                    for f in i['barcodes'][b]['Viruses']['filtspecies']:
     	               print("{0},{1},{2},{3},{4}".format(
     					i['sample_ID'],
                                        	b,
     					txname(int(f)),
     			i['barcodes'][b]['Viruses']['filtspecies'][f]['bases'],
     			i['barcodes'][b]['Viruses']['filtspecies'][f]['reads']))    
            sys.exit() 
    
        if kdict['all_species'] == True:
            print("{0},{1},{2},{3},{4}".format(
                         'Run_name',
                         'Barcode',
                         'Taxid',
                         'Bases',
                         'Reads'))
            h=m.someStats()
            printr(h,'Bacteria')
            h=m.someStats()
            printr(h,'Viruses')
            sys.exit()


        if kdict['map'] == True:
             print("{0},{1},{2},{3},{4},{5},{6}".format(
                         'Run_name',
                         'Barcode',
                         'Taxid',
                         'chromosome',
                         'covBread_1x_percent',
                         'avCov',
                         'cov_stdv'))
             h=m.someStats()
             printm(h,'Bacteria')
             h=m.someStats()
             printm(h,'Viruses')
             sys.exit()
    
        h=m.someStats() 
        print("sample_ID\tguuid\tbarcode\thuman bases\thuman reads\tbacterial bases\tbacterial reads\tviral bases\tviral reads\ttotal bases\ttotal reads")
        for i in h:
            for b in i['barcodes']:
                print("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}".format(i['sample_ID'],
                i['_id'],
                b,
                i['barcodes'][b]['Human']['bases'],
                i['barcodes'][b]['Human']['reads'],
                i['barcodes'][b]['Bacteria']['bases'],
                i['barcodes'][b]['Bacteria']['reads'],
                i['barcodes'][b]['Viruses']['bases'],
                i['barcodes'][b]['Viruses']['reads'],
                i['barcodes'][b]['total']['bases'],
                i['barcodes'][b]['total']['reads']))
    
    if kdict['guuids'] != None:
        kdict['guuids']=[ObjectId(g) for g in kdict['guuids'].split(',')]
        m=getRuns(**kdict)
        h=m.getGuuids()
        for i in h: pprint.pprint(i)#['sample_ID'],i['_id'],i['1D_Mbases'])
    if kdict['all'] == True:
        m=getRuns(**kdict)
        h=m.allStats()
        print("sample_ID\tchannels\tmb\tmedia_length\tmp_per_ch\tdate")
        for i in h:
            bases=i['barcode_summaries']['total']['1D_Mbases']
            channels=i['activeChannels']
            print("{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(i['sample_ID'],
                                        channels,
                                        bases,
                                        i['barcode_summaries']['total']['median_read_length(kb)'],
                                        bases/float(channels),
                                        i['date_started']))

def getRunsArgs(parser):
    parser.add_argument('-t', '--table_name', required=False,default='crumpit_summaries',
                         help='crumpit_summaries is the name of the default db setup')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                         help='port address for mongoDB database')
    parser.add_argument('-s', '--samples', required=False,type=str,
                         help='sample or list of samples, comma seperated')
    parser.add_argument('-g', '--guuids', required=False,type=str,
                         help='guuids or list of guuids, comma seperated')
    parser.add_argument('-all', '--all', required=False,action='store_true',
                         help='print all runs')
    parser.add_argument('-fp', '--species', required=False,action='store_true',
                         help='view filtered species breakdown')
    parser.add_argument('-sp', '--all_species', required=False,action='store_true',
                         help='view all species breakdown')
    parser.add_argument('-m', '--map', required=False,action='store_true',
                         help='view all mapped chromosomes breakdown')

    return parser

def getRunsRun(opts):
    kdict=vars(opts)
    run(kdict)


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='get run details from crumpitdb')
    parser = getRunsArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    getRunsRun(opts)


