#!/usr/bin/env python3
import sys
import subprocess
import ete3
import json
from tqdm import tqdm
import os.path
import os
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO
import gzip
ncbi = ete3.NCBITaxa()
import logging

logging.basicConfig(level=logging.DEBUG, filename='crumpit.log')

def getDescendants(taxid,refid):
    dnts = ncbi.get_descendant_taxa(taxid, intermediate_nodes=True)
    dnts.append(taxid)
    return {key: refid for key in dnts}

def getSpecies(taxes):
    try:
        l=ncbi.get_lineage(taxes)
        r=ncbi.get_rank(l)
        return {'species':i for i in l if r[i] == 'species'}
    except:
        return {'species':'errorId'}

def save_obj(obj, name ):
    with open( name , 'w') as f:
        json.dump(obj,f)

def load_obj(name ):
    try:
        with open( name , 'r') as f:
            return json.load(f)
    except:
        return {}

class getRefs:
    def __init__(self, domain, db):
        self.domain=domain  # virus, bacteria etc
        self.db=db          # refseq etc
        self.rp='{0}_refs.json'.format(self.domain)
        #self.d=load_obj(self.rp)
        self.d={}
        self.fails={}
        self.assmblyfile="{0}_{1}_assembly.txt".format(self.db,self.domain)
        #self.run()

    def run(self):
        self.makeFol() # make the output directory if new start
        self.pullAssembly() # get ncbi spreadsheet on 
        self.assmblyDict() # turn to nested dictionary with only na, ref or rep genomes
        self.whichRef() # choose refs with multiple sub species
        self.reflist() # create ref lookup dict and also download refs 
        save_obj(self.d,self.rp) # save ref
        self.mergeRefJsons()

    def mergeRefJsons(self):
        refJson = load_obj('refs.json')
        newJson = {**refJson, **self.d}
        save_obj(newJson,'refs.json')

    def makeFol(self):
        fol=self.domain
        if not os.path.exists(fol):
            os.makedirs(fol)


    def pullAssembly(self):
        l=["rsync", "rsync://ftp.ncbi.nlm.nih.gov/genomes/{0}/{1}/assembly_summary.txt".format(self.db,self.domain), self.assmblyfile]
        assmbly=subprocess.check_output(l)
    
    def dlGenome(self,ftp):
        name='{0}_genomic.fna.gz'.format(ftp.split('/')[-1])
        if os.path.isfile("{0}/{1}".format(self.domain,name)): return name
        rsync='rsync' + ftp[3:]
        l=['rsync','-avP','{0}/{1}'.format(rsync,name),"{0}/{1}".format(self.domain,name)]
        try:
            genome=subprocess.check_output(l)
        except subprocess.CalledProcessError as e:
            self.fails[ftp]=e.output
        return name

    def addTaxa(self,t,r):
        self.d.update(getDescendants(t,r))

    def assmblyDict(self): # turn to nested dictionary with only ref or rep genomes
        self.ad={}
        f=open(self.assmblyfile,'rt').read().split('\n')
        headers=f[1].split('\t')
        for line in f[2:-1]:
            l=line.split('\t')
            if line.split('\t')[11] != 'Complete Genome': continue
            #if line.split('\t')[4] == 'na': continue
            if line.split('\t')[19][:3]!='ftp': continue
            d={headers[i]:l[i] for i in range(len(l))}
            self.ad.setdefault(getSpecies(d['taxid'])['species'],{}).setdefault(d['# assembly_accession'],d)
        save_obj(self.ad,'refs_ad.json')

    def reflist(self): # create look up table and download refs
        for t in tqdm(self.refdic):
            ftp=self.refdic[t]['ftp_path']
            refid='{0}/{1}_genomic.fna.gz'.format(self.domain,ftp.split('/')[-1]) 
            self.addTaxa(t,refid)
            refid=self.dlGenome(ftp)

    def whichRef(self): # make dictionary of chosen referencces
        self.refdic={}
        for t in self.ad:
            if t == 'errorId': continue
            if len(self.ad[t]) >=2 :
                if not all(self.ad[t][acc]["refseq_category"] == "representative genome" for acc in self.ad[t]):
                    # get only representative genome if available
                    if not all(self.ad[t][acc]["refseq_category"] != "representative genome" for acc in self.ad[t]):
                        for acc in self.ad[t]:
                            if self.ad[t][acc]["refseq_category"] == "representative genome": 
                                self.refdic.setdefault(t,self.ad[t][acc])
                elif all(self.ad[t][acc]["refseq_category"] == "representative genome" for acc in self.ad[t]):
                    #choose the first representative genome  if more than one in list (pretty much random)
                    k=list(self.ad[t].keys())[0]
                    self.refdic.setdefault(t,self.ad[t][k]) 
                elif all(self.ad[t][acc]["refseq_category"] == "na" for acc in self.ad[t]):
                    #choose the first in list (pretty much random)
                    k=list(self.ad[t].keys())[0]
                    self.refdic.setdefault(t,self.ad[t][k])
                else: #get the reference strain
                    for acc in self.ad[t]:
                        if self.ad[t][acc]["refseq_category"] == "reference genome": 
                            self.refdic.setdefault(t,self.ad[t][acc])

            elif len(self.ad[t]) > 0:
                k=list(self.ad[t].keys())[0]
                self.refdic.setdefault(t,self.ad[t][k])
        save_obj(self.refdic,"{0}_{1}_whichref.json".format(self.db,self.domain))

    def makeConTable(self):
        self.convDict() # turn to nested dictionary with only ref or rep genomes
        #save_obj(self.conv,self.rp) # save ref
        self.getIds()

    def returnIds(self,seqfile):
        for seq in SeqIO.parse(gzip.open(seqfile,'rt'),'fasta'):
            yield seq.id

    def getIds(self):
        with open('{0}_conversion_table.txt'.format(self.domain),'wt') as outf:
            for sf in tqdm(self.conv):
                try:
                    for i in list(self.returnIds(sf)):
                        outf.write("{0}\t{1}\n".format(i,self.conv[sf]))
                except:
                    logging.exception("crumpit refmaker - conversion table ERROR:")


    def convDict(self): # turn to nested dictionary with only ref or rep genomes
        self.conv={}
        f=open(self.assmblyfile,'rt').read().split('\n')
        headers=f[1].split('\t')
        for line in f[2:-1]:
            l=line.split('\t')
            if line.split('\t')[11] != 'Complete Genome': continue
            #if line.split('\t')[4] == 'na': continue
            if line.split('\t')[19][:3]!='ftp': continue
            d={headers[i]:l[i] for i in range(len(l))}
            ftp=d['ftp_path']
            refid='{0}/{1}_genomic.fna.gz'.format(self.domain,ftp.split('/')[-1])
            self.conv[refid]=d['taxid']


def refsGetArgs(parser):
    parser.add_argument('-d', '--domain', required=True,
                  help='specify domain to download, bacteria, viral etc')
    parser.add_argument('-download', '--download', required=False,action='store_true',
                  help='download sequences for reference, default=True')
    parser.add_argument('-ct', '--conversionTable', required=False,action='store_true',
                  help='make centrifuge conversion table, default=True')
    return parser

def refMakerRun(opts):
    c=getRefs(opts.domain,'refseq')
    if opts.download == True:
        c.run()
    if opts.conversionTable == True:
        c.makeConTable()

            
if __name__=="__main__":
    parser = ArgumentParser(description='Download or update references from refseq')
    parser=refsGetArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    c=getRefs(opts.domain,'refseq')
    #c=getRefs('viral','refseq')
