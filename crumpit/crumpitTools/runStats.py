#!/usr/bin/env python3
import sys
import numpy as np
from pymongo import MongoClient
import pymongo
from bson.objectid import ObjectId
from crumpitTools.mongdbConnect import mdbConnect
from argparse import ArgumentParser
import operator
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import time
import json
import seaborn as sns
sns.set_style("darkgrid")

class runStats(mdbConnect):
    def __init__(self,**kwargs):
        allowed=['table_name']
        mdbConnect.__init__(self,**kwargs)
        for k, v in kwargs.items():
            if k in allowed: setattr(self, k, v)
        self.collection = self.db.alba_summary
        self.times,self.bases,self.quals={},{},{}
        self.getRunMeta()

    def getRunMeta(self):
        self.run_ids= self.collection.distinct('run_id')
        self.channels=range(0,513)  #self.collection.distinct('channel')
        self.channelStats={str(c):{'bases':0,'reads':0} for c in self.channels}
        f1=self.collection.find_one()
        try:
            self.first_doc=ObjectId(f1['_id']).generation_time
        except:
            self.first_doc=None

    def getSampleStats(self,last=False):
        '''return lits of reads lengths, qualitity scores and timings'''
        tmpl='sequence_length_template'
        #tmpl='num_called_template'
        # example of indexing but not needed if finding all docs
        #self.collection.create_index([(tmpl,pymongo.ASCENDING),
        #                            ('mean_qscore_template',pymongo.ASCENDING),
        #                            ('start_time',pymongo.ASCENDING)])
        if last==False: hce = self.collection.find()
        else: hce = self.collection.find({"_id": {"$gt": self.last}})
        # run through all documents and compile reads,
        # could be faster with aggregrate framework but need to figure that out
        for h in hce:
            if h[tmpl] == tmpl: continue # ignore headers
            #if h['passes_filtering'] == 'False': continue
            try:
                plex=h['barcode_arrangement']
            except:
                plex='nobarcode'
            try:
                self.bases.setdefault(plex,[]).append(int(h[tmpl]))
                self.quals.setdefault(plex,[]).append(float(h['mean_qscore_template']))
                self.times.setdefault(int(float(h['start_time'])),[]).append(int(h[tmpl]))
            except:
                pass

            try:
                self.channelStats[str(h['channel'])]['reads']+=1
                self.channelStats[str(h['channel'])]['bases']+=int(h[tmpl])
                self.last=ObjectId(h['_id'])
            except:
                pass

        return self.bases,self.quals,self.times

    def getTimes(self,d):
        sorted_x = sorted(d.items(), key=operator.itemgetter(0))
        b,r=0,0
        x,y=[],[]
        for i in sorted_x:
            p=i[0]
            b+=sum(i[1])
            r+=1
            x.append(p)
            y.append(b)
        return x,y

    def visTimings(self,td):
        fig,ax=plt.subplots()
        x1,y1=self.getTimes(td)
        d={'runName':self.table_name,'times':x1,'bases':y1}
        with open("{0}_times.json".format(self.table_name),'w') as fp:
            json.dump(d,fp)

        ax.plot(x1,y1)

        x_scale=60*60
        y_scale=1000000
        ax.set_autoscaley_on(False)
        ticks = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(int(x/x_scale)))
        ticks_y = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x/y_scale))
        ax.yaxis.set_major_formatter(ticks_y)
        #plt.ylim(0,y1[-1])
        ax.xaxis.set_major_formatter(ticks)
        plt.xlim(0,60*60*48)

        plt.xlabel('Hours after run start',fontsize=14)
        plt.ylabel('Mega bases',fontsize=14)

        plt.savefig("{0}_bases_over_time.pdf".format(self.table_name),dpi=600)
        plt.close()

    def qualViolins(self,q):
        fig,ax=plt.subplots()
        vq={k: v for k, v in q.items() if len(v) > 100}
        ax.violinplot(list(vq.values()))
        ax.set_xticks(list(range(len(vq))))
        ax.set_xticklabels(list(vq.keys()))
        for tick in ax.get_xticklabels(): tick.set_rotation(45)
        plt.ylabel('Phred score')
        plt.savefig("{0}_quality_violins.pdf".format(self.table_name),dpi=600)
        plt.close()

    def run(self,b,q,t):
        t="barcode\tbases\treads\tAverage Phred\tLongest\tShortest\tMedian\tAverage\n"
        #for bar in b:
        #    print("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}".format(bar,sum(b[bar]),len(b[bar]),sum(q[bar])/len(b[bar]),max(b[bar]),min(b[bar]),np.median(b[bar]),sum(b[bar])/len(b[bar])))
        reads=[item for sublist in(list(b.values())) for item in sublist]
        qs=[item for sublist in(list(q.values())) for item in sublist]
        if len(reads)>1:
            t+="{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n".format('Total',sum(reads),len(reads),sum(qs)/len(reads),max(reads),min(reads),np.median(reads),sum(reads)/len(reads))
        with open("{0}_runstats.tsv".format(self.table_name),'wt') as outfile:
            outfile.write(t)

    def watcher(self):
        while True:
            try:
                b,q,t=self.getSampleStats(last=True)
                self.run(b,q,t)
                self.visTimings(t)
                print('sleep 2 mins')
                time.sleep(120)
            except KeyboardInterrupt:
                sys.exit()



if __name__ == "__main__":
        # args
        parser = ArgumentParser(description='centrifuge detail pusher')
        parser.add_argument('-t', '--table_name', required=True,
                             help='Specify sample name as used in mongoDB.')
        parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
        parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')

        opts, unknown_args = parser.parse_known_args()
        ktdict=vars(opts)
        # run
        m=runStats(**ktdict)
        b,q,t=m.getSampleStats()
        m.run(b,q,t)
        m.visTimings(t)
        #m.qualViolins(q)
        m.watcher()
