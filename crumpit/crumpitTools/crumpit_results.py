#!/usr/bin/env python3
from crumpitTools.cent_stats import mStats
from crumpitTools.map_stats import mpStats
from crumpitTools.crumpit_view import visJson
from crumpitTools.runStats import runStats
from crumpitTools.uploadCRuMPIT import getCrums,pushCrums
from crumpitTools.f5info import runInfoSum
from crumpitTools.pushPile import runPullPile
from argparse import ArgumentParser, SUPPRESS
import sys
import curses
from curses import wrapper
import time
from datetime import datetime,timedelta
import json
from ete3 import NCBITaxa
import os
ncbi = NCBITaxa()
import subprocess
from pathlib import Path
import logging
import warnings
warnings.filterwarnings("ignore")

logging.basicConfig(level=logging.DEBUG, filename='crumpit_results.log')


def getVers(h,b):
    try:
        os.chdir(h)
        l=subprocess.check_output(["git", "describe", "--always"],universal_newlines=True).strip()
        os.chdir(b)
    except:
        l="Verson not found"
    return l

def save_obj(obj, name ):
    with open( name , 'w') as f:
        json.dump(obj,f)

class crumpitResult:
    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            #if type(k) or type(v) != MethodType:
            setattr(self, k, v)
        self.kwargs=kwargs
        self.kwargs['git_vers_or_commit']=getVers(self.home,self.base)

        taxes={"Bacteria":2,"Human":9606,"Viruses":10239}
        self.centStats=mStats(self.sample_name,thresh=float(self.cent_score),mdbip=self.ip,mdbport=self.port,taxes=taxes)
        #self.mp=mpStats(self.sample_name,score=self.mapq,mdbip=self.ip,mdbport=self.port,thresh=self.cent_score)
        self.runStats=runStats(table_name=self.sample_name,ip=self.ip,port=self.port)
        self.runInfo=runInfoSum(run_name=self.sample_name,ip=self.ip,port=self.port)

    def cfStats(self):
        #m.basicBases=dict(zip(taxes, map(m.countBases,taxes.values()))) # run count on interested taxes
        m=self.centStats
        m.countBases()
        # get Species breakdown for a kingdon
        for b in m.barcodes:
            m.barcodes[b]['Bacteria']['filtspecies']={str(k): v for k, v in m.barcodes[b]['Bacteria']['spBases'].items() if v['bases'] > (float(m.barcodes[b]['Bacteria']['bases'])/100)*float(self.cent_bases)}
            m.barcodes[b]['Viruses']['filtspecies']={str(k): v for k, v in m.barcodes[b]['Viruses']['spBases'].items() if v['bases'] > (float(m.barcodes[b]['Viruses']['bases'])/100)*float(self.cent_bases)}

        return m.barcodes

    def mappingStats(self,taxids,rds_needed=1,bar=None):
        mp=mpStats(self.sample_name,score=self.mapq,mdbip=self.ip,mdbport=self.port,thresh=self.cent_score)
        mp.bases=float(rds_needed)
        mp.barcodes=str(bar)
        #mp.visRatioTimings(taxids,showChart=ags.chart,ut=ags.until)
        return mp.visTimings(taxids,showChart=self.chart,ut=self.until)

    def gName(self,t):
        if t in ['unclassified','total','Bacteria','Human','Viruses']: return t
        taxid2name = ncbi.get_taxid_translator([t])
        return taxid2name[t]

    def coverStats(self,tax,bar):
        mpileupFile='mpileup/{0}/{1}.mpileup'.format(bar,tax)
        my_file = Path( mpileupFile )
        cov =  None
        if not my_file.is_file(): return cov
        try:
            cov = {'chroms': runPullPile(tax,
                    self.sample_name,
                    bar,
                    self.refbase,
                    ip=self.ip,
                    port=self.port,
                    pf=mpileupFile)}
        except:
            logging.exception("results cov ERROR:")

        return cov

    def run(self):
        # get centrifuge statistics for entire run broken down into barcodes
        barcodes=self.cfStats()
        # get mapping stats
        for b in barcodes:
            # mapping stats
            fsList=list(barcodes[b]['Bacteria']['filtspecies'].keys())
            if '1763' in fsList:
                fsList.remove('1763')
#            fsList.append('1763') # always look for Mycobacterium

            mps=self.mappingStats(fsList,
                    rds_needed=(barcodes[b]['Bacteria']['reads']/100.0)*float(self.minimum_map_bases),
                    bar=b)
            
            barcodes[b]['Bacteria']['minimap2']=mps
            
            vps=self.mappingStats(list(barcodes[b]['Viruses']['filtspecies'].keys()),
                    rds_needed=(barcodes[b]['Viruses']['reads']/100.0)*float(self.minimum_map_bases),
                    bar=b)
            
            barcodes[b]['Viruses']['minimap2']=vps
            # coverage stats
            #if barcodes[b]['Bacteria']['minimap2'] != None: 
            #    for tax in list(barcodes[b]['Bacteria']['minimap2'].keys()):
            #        #if barcodes[b]['Bacteria']['filtspecies'][tax] != None:
            #        c=self.coverStats(tax,b)  
            #        if c != None: barcodes[b]['Bacteria']['minimap2'][tax].update(c)

            #if barcodes[b]['Viruses']['minimap2'] != None: 
            #    for tax in list(barcodes[b]['Viruses']['minimap2'].keys()):
            #        #if barcodes[b]['Viruses']['filtspecies'][tax] != None:
            #        c=self.coverStats(tax,b)
            #        if c != None: barcodes[b]['Viruses']['minimap2'][tax].update(c)



        self.kwargs['barcodes']=barcodes
        # get runStats for basecalling info
        if hasattr(self.runStats,'last'):
            b,q,t=self.runStats.getSampleStats(last=True)
        else: b,q,t=self.runStats.getSampleStats()
        self.runStats.run(b,q,t)
        self.runStats.visTimings(t)

        # get run info summaries
        self.runInfo.getSummary()
        self.kwargs.update(self.runInfo.sets)

        # clean data and save to json
        allowedTypes=[str,int,list,dict,float,set]
        self.saveTypes={k:self.kwargs[k] for k in self.kwargs if type(self.kwargs[k]) in allowedTypes}
        save_obj(self.saveTypes,'{0}_barcodes.json'.format(str(self.sample_name)))

        # push summaries
        gc=getCrums(table_name=self.sample_name,ip=self.ip,port=self.port,crumDict=self.saveTypes,runData={})
        gc.getRunData(runData=(b,q,t),runStats=self.runStats)
        if self.test == True:
            pc=pushCrums(table_name='crumpit_summaries_test',ip=self.ip,port=self.port)
        else:
            pc=pushCrums(table_name='crumpit_summaries',ip=self.ip,port=self.port)
        #import pprint
        #pprint(gc.allData)
        pc.pushData(gc.allData)
        return barcodes

def visualise(rpt,w,w2):
    c,crum=visJson(rpt)
    reps=c.split('\n')
    reps2=crum.split('\n')
    w.clrtobot()
    w2.clrtobot()
    for i in range(len(reps)):
        w.addstr(i+1, 1, reps[i])
        w.clrtoeol()
    for i in range(len(reps2)):
        if i+1 > 39: continue
        w2.addstr(i+1, 1, reps2[i])
        w2.clrtoeol()
    w.clrtoeol()
    w2.clrtoeol()


def crGetArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True,
                             help='Specify sample name as used in mongoDB.')

    #optionals
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
    parser.add_argument('-c', '--cent_score', required=False, default=150, type=int,
                             help='Centrifuge score cuttoff threshold. Default=150')
    parser.add_argument('-cb', '--cent_bases', required=False, default=10.0, type=float,
                             help='Centrifuge bases percentage cuttoff threshold. Default=10')
    parser.add_argument('-r', '--minimum_map_bases', required=False, default=1, type=float,
                             help='Mininum bacterial bases percentage cuttoff threshold. Default=1')
    parser.add_argument('-refs', '--refbase', required=True,
                             help='Location of reference sequences')
    parser.add_argument('-q', '--mapq', required=False, default=50, type=int,
                             help='Mininum mapq quality threshold for mapping. Default=50')
    parser.add_argument('-ch', '--chart',required=False, default='n',
                             help='chart on(y) or off(n), default=n')
    parser.add_argument('-ut', '--until',required=False, default=60*60*24*2, type=int,
                             help='Time in seconds after start of run to show')
    parser.add_argument('-w', '--watch', required=False, default='y',type=str,
                             help='y to watch run, n to only already analyse sequenced files, (default=y)')
    parser.add_argument('-test', '--test',required=False,action='store_true',
        			         help='usef for testing CRuMPIT, stores summaries in crumpit_summaries_test')
    #opts, unknown_args = parser.parse_known_args()
    #parser.add_argument('-o', '--outf', required=False, type=str, default="{0}_results.tsv".format(opts.sample_name),
    #                         help='output file for classification results in tab format')
    #opts, unknown_args = parser.parse_known_args()
    return parser

def crRun(opts):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    cwd = os.getcwd()

    # run
    # get cent stats inc
    #     1: Dict of bacterial species, inc reads lengths, abover classification
    #     2: number of bacteria reads classified above threshold
    #     3: Dict of basic stats (human, bacteria, vir, class,unclas, total)

    kditc=vars(opts)
    kditc['home']=dir_path
    kditc['base']=cwd
    cr=crumpitResult(**kditc)

    if opts.watch=='y':
        begin_x = 0; begin_y = 0
        height = 15; width = 85
        stdscr = curses.initscr()
        win0 = curses.newwin(height, width, begin_y, begin_x)
        win1 = curses.newwin(8, 60, 0, 85)
        win2 = curses.newwin(32, 60, 8, 85)
        win3 = curses.newwin(40, width, 15, 0)
        wins=[win0,win1,win2,win3]
        stdscr.refresh()
        curses.noecho()
        curses.cbreak()

        # run nextflow
        initime=datetime.now()
        try:
            while True:
                try:
                    #cr=crumpitResult(**kditc)
                    rp=cr.run()
                    visualise(rp,win0,win3)
                    win2.addstr(1,1,"Nextflow logs")
                    for w in wins: w.border(0), w.refresh()
                except:
                    logging.exception("results run ERROR:")
                time.sleep(10)

        except KeyboardInterrupt:
            curses.echo()
            curses.nocbreak()
            curses.endwin()
            sys.exit()
    else:
        cr=crumpitResult(**kditc)
        rp=cr.run()
    #with open(kditc['outf'],'wt') as outf:
    #    outf.write(rp)

if __name__=="__main__":
    # args
    parser = ArgumentParser(description='Classflow clinical metagenomics classification workflow')
    parser=crGetArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    crRun(opts)
