#!/usr/bin/env python3
import sys
import json
import os
import subprocess
from Bio import SeqIO
import gzip
from argparse import ArgumentParser, SUPPRESS
import os.path
from tqdm import tqdm
import json
import numpy as np
import pandas as pd
import operator

def rolling_window(a,window):
    df = pd.DataFrame(a)
    df['avg'] = df.rolling(window).mean()
    y = df['avg'].values.tolist()[0::window]
    x = [i*window for i in range(len(y))]
    return x,y

bases = ['A',
        'T',
        'C',
        'G',
        'a',
        't',
        'c',
        'g',
        '.',
        ',']

class callPile:
    def __init__(self,pf,refs,out):
        self.pileFile = pf
        self.refs = refs
        self.out = out
        self.refInfo()

    def getMpileup(self):
        '''return dictionary of each mpileup file'''
        posts=[]
        with open(self.pileFile,'rt') as pileUp:
            for line in pileUp:
                l=line.split('\t')
                if len(l) == 6:
                    p={ 'pos':int(l[1]),
                        'ref':str(l[2]),
                        'cov':int(l[3]),
                        'seq':list(str(l[4])),
                        'qual':list(str(l[5]).replace('\n','')),
                        'chrom':str(l[0])}
                elif len(l) == 5:
                    p={ 'pos':int(l[1]),
                        'ref':str(l[2]),
                        'cov':int(l[3]),
                        'seq':list(str(l[4]).replace('\n','')),
                        'chrom':str(l[0])}
                else: continue
                yield p

        #return posts

    def load_obj(self, name ):
        with open( name , 'r') as f:
            return json.load(f)


    def refInfo(self):
        self.refStats,self.refDescript={},{}
        refFile=self.refs
        with open(refFile,'rt') as inF:
            for seq in SeqIO.parse(inF,'fasta'):
                chrom=str(seq.id)#.replace('.','_')
                self.refStats[chrom] = { 'len' : len(seq.seq) , 
                        'covpos' : np.zeros(len(seq.seq) ),
                        'baspos' : np.chararray(len(seq.seq) ) }
                self.refStats[chrom]['baspos'][:] = 'N'
                self.refDescript[chrom] = seq.description


    def consCall(self,seq):
        if len(seq) == 1 and seq[0] not in bases:
            return 'N'

        indel = False
        indelN = 0
        base={}
        indels={ '-' : [] , '+' : [] }
        for b in bases: base[b] = 0

        for s in seq:
            if s == '-' or s == '+':
                indel = True
                indelOp = s
                continue

            if indel == True and indelN == 0:
                indelN = int(s)
        
            elif indel == True and indelN > 0:
                indels[indelOp].append(s)
                indelN -= 1
                if indelN == 0:indel = False

            else:                
                if s in bases:
                    base[s.upper()]+=1


        return max(base.items(), key=operator.itemgetter(1))[0]
        
        

    def calcCover(self,hce,chrom):
        for h in hce:
            chrom=h['chrom']#.replace('.','_')
            self.refStats[chrom]['covpos'][h['pos'] -1 ] = h['cov'] 
            self.refStats[chrom]['baspos'][h['pos'] -1 ] = self.consCall( h ['seq'] )


    def getCoverage(self):
        if self.pileFile!=None:
            hce=self.getMpileup()
            chrom=None
            self.calcCover(hce, chrom)
        for chrom in self.refStats:
            self.refStats[chrom]['x1']=len(np.where( self.refStats[chrom]['covpos'] > 0 )[0])
            self.refStats[chrom]['x5']=len(np.where( self.refStats[chrom]['covpos'] > 4 )[0])
            if self.refStats[chrom]['x1'] > 0:
                self.refStats[chrom]['covBread_1x_percent']=float(self.refStats[chrom]['x1']/self.refStats[chrom]['len'])*100
                self.refStats[chrom]['avCov']=np.sum(self.refStats[chrom]['covpos'])/self.refStats[chrom]['x1']
                self.refStats[chrom]['cov_stdv']=np.std(self.refStats[chrom]['covpos'])
            else:
                self.refStats[chrom]['covBread_1x_percent']=0
                self.refStats[chrom]['avCov']=0
                self.refStats[chrom]['cov_stdv']=0
            #self.refStats[chrom]['covpos']=list(self.refStats[chrom]['covpos'])
            #del self.refStats[chrom]['covpos']
            #del self.refStats[chrom]['baspos']


    def saveCons(self):
        s=''
        with open(self.out,'wt') as outf:
            for chrom in self.refStats:
                if self.refStats[chrom]['covBread_1x_percent'] < 50: continue

                s+='>{0}\n{1}\n'.format( chrom,
                        self.refStats[chrom]['baspos'].tostring().decode('utf-8') )

            outf.write(s)


def callPile_args(parser):
    parser.add_argument('-pf', '--pile_file',required=True,
                             help='pileup file')
    parser.add_argument('-r', '--ref', required=True,
                             help='reference sequences mapped to')
    parser.add_argument('-o', '--out', required=True,
                             help='output file to write to')
    return parser

def callPile_run(opts):
    cp = callPile( opts.pile_file, opts.ref, opts.out )
    cp.getCoverage()
    cp.saveCons()        

if __name__=="__main__":
    # args
    parser = ArgumentParser(description='generate consensus from mpileup')
    parser = callPile_args(parser)
    opts, unknown_args = parser.parse_known_args()
    # run script
    callPile_run(opts)

