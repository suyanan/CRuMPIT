#!/usr/bin/env python3
import configparser
import sys
import pprint

class cmptConf:
    def __init__(self,conf=None):
        self.config = configparser.ConfigParser()
        if conf != None: self.readConfig(conf)

    def readConfig(self,conf):
        self.config.read(conf)

if __name__ == "__main__":
    c=cmptConf(conf=sys.argv[1])
    d=dict(c.config.items('crumpit'))
    pprint.pprint(d)
