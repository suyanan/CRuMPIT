#!/usr/bin/env python3
import sys
import subprocess
import os
import glob
from argparse import ArgumentParser, SUPPRESS
from crumpitTools.batcher import batcher
from crumpitTools.runNextflow import nfWorkflow
from crumpitTools.config_parser import cmptConf
import curses
from curses import wrapper
import time
from datetime import datetime,timedelta
from crumpitTools.crumpit_results import crumpitResult
from crumpitTools.crumpit_view import visJson
from crumpitTools.rerun_neglected import rerun
import logging

logging.basicConfig(level=logging.DEBUG, filename='crumpit_call.log')


class crumpitCall:
    def __init__(self):
        try:
            c=cmptConf('/etc/crumpit.config')
            wfconfig=dict(c.config.items('crumpit'))
            self.wfconfig=wfconfig
        except:
            print('no config file in /etc/')
            sys.exit()

    def runGridWatch(self,opts,bt):
        '''function to run for Gridion (basecalled) fastq files. Keeps nextflow running for 48 hours "watching" a folder'''
        initime=datetime.now()
        #bt=batcher(opts.fast5s,opts.bdir,batches=opts.batch_numbers,batchsize=1,fileType='fastq',gridFiles=opts.gridFiles)
        #bt.runGrid()
        self.nf.run()
        bt.runWatch(grid=True)
        self.wfconfig['refbase']=opts.refbase
        cr=crumpitResult(**self.wfconfig)
        try:
            while datetime.now()-initime < timedelta(hours=float(self.wfconfig['watchHours'])):
                try:
                #cr=crumpitResult(**self.wfconfig)
                    rp=cr.run()
                except:
                    logging.exception("crumpit call - results run ERROR:")
                time.sleep(60)
        except KeyboardInterrupt:
            bt.stopWatch()
            self.nf.nfpipe.kill()
            sys.exit()
        self.nf.nfpipe.kill()
        bt.stopWatch()
        self.rerunNeglected(opts,cr)
        sys.exit()

    def rerunNeglected(self,opts,cr):
        # get missed batches
        rrwfconfig=self.wfconfig
        d2={'dirs':self.wfconfig['base'],'watch':False,'bdir':'{0}/tmp'.format(self.wfconfig['base'])}
        rrwfconfig.update(d2)
        rr=rerun(**d2)
        rr.run()
        if rr.failedN > 0:
            # run nextflow without watch
            rrnf=nfWorkflow(**rrwfconfig)
            rrnf.run()
            rp=cr.run()

    def outputDetail(self):
        try:
            fols=os.listdir(self.wfconfig['base']+'/fastqs')
        except Exception as e:
            return 0

        return len(fols)

    def visualise(self,rpt,w,w2):
        c,crum=visJson(rpt)
        reps=c.split('\n')
        reps2=crum.split('\n')
        w.clrtobot()
        w2.clrtobot()
        for i in range(len(reps)):
            w.addstr(i+1, 1, reps[i])
            w.clrtoeol()
        for i in range(len(reps2)):
            if i+1 > 39: continue
            w2.addstr(i+1, 1, reps2[i])
            w2.clrtoeol()
        w.clrtoeol()
        w2.clrtoeol()
        #w.refresh()

    def nflowStatus(self):
        if self.nf.nfpipe.poll() is None:
            message="Running"
            colour=9
        else:
            message="Stopped"
            colour=44
        return message,colour

    def batchInfo(self,w,initime):
        # upper right hand pane on Run information
        now=datetime.now()
        message,colour=self.nflowStatus()
        w.addstr(1,1,"Batches found:\t{0}".format(self.bt.batches))
        w.addstr(2,1,"Batches basecalled:\t{0}".format(self.outputDetail()))
        w.addstr(4,1,"Time initiated:\t{0}".format(initime))
        w.addstr(5,1,"Time of last analysis:\t{0}".format(now))
        w.addstr(6,1,"Time from initiation:\t{0}".format(now-initime))
        w.addstr(3,1,"Nexflow status:\t{0}".format(message))

    def duringRun(self,stdscr):
        #setup screen
        begin_x = 0; begin_y = 0
        height = 15; width = 85
        stdscr = curses.initscr()
        win0 = curses.newwin(height, width, begin_y, begin_x)
        win1 = curses.newwin(8, 60, 0, 85)
        win2 = curses.newwin(32, 60, 8, 85)
        win3 = curses.newwin(40, width, 15, 0)
        wins=[win0,win1,win2,win3]
        stdscr.refresh()
        curses.noecho()
        curses.cbreak()

        # run nextflow
        initime=datetime.now()
        self.nf.run(crswin=win2)
        self.bt.runWatch()
        self.cr=crumpitResult(**self.wfconfig)
        try:
            while True:
                #self.cr=crumpitResult(**self.wfconfig)
                rp=self.cr.run()
                self.visualise(rp,win0,win3)
                self.batchInfo(win1,initime)
                win2.addstr(1,1,"Nextflow logs")
                for w in wins: w.border(0), w.refresh()
                time.sleep(30)
        except KeyboardInterrupt:
            self.bt.stopWatch()
            self.nf.nfpipe.kill()
            curses.echo()
            curses.nocbreak()
            curses.endwin()
            sys.exit()

    def duringRunNoVis(self):
        #setup screen
        # run nextflow
        initime=datetime.now()
        self.nf.run()
        self.bt.runWatch()
        self.cr=crumpitResult(**self.wfconfig)
        try:
            while True:
                #self.cr=crumpitResult(**self.wfconfig)
                rp=self.cr.run()
                time.sleep(30)
        except KeyboardInterrupt:
            self.bt.stopWatch()
            self.nf.nfpipe.kill()
            sys.exit()

    def run(self,opts):
        c=cmptConf('/etc/crumpit.config')
        wfconfig=dict(c.config.items('crumpit'))
        cwd = os.getcwd()
        dir_path = os.path.dirname(os.path.realpath(__file__))
        # overwrite config params with argparse
        # THIS NEEDS CHANGING
        wfconfig.update(vars(opts))
        wfconfig['base']=cwd
        wfconfig['home']=dir_path
        self.wfconfig=wfconfig
        if not os.path.exists(opts.bdir):
            os.makedirs(opts.bdir)
        # run
        # first run batcher to make batch files of fast5 file lists
        if opts.grid==False:
            if opts.sample_name==False:
                print('Need sample name for MinION run')
                sys.exit()
            self.bt=batcher(opts.fast5s,opts.bdir,batches=opts.batch_numbers,batchsize=opts.batchsize)
            self.bt.run()
        # second start nextflow workflow
        if opts.grid==True:
            bt=batcher(opts.fast5s,opts.bdir,batches=opts.batch_numbers,batchsize=1,fileType='fastq',gridFiles=opts.gridFiles,porechop=opts.porechop)
            bt.runGrid()
            if opts.watch==False:
                self.nf=nfWorkflow(**wfconfig)
                self.nf.run()
                sys.exit()
        self.nf=nfWorkflow(**wfconfig)
        # Third, if watching, generate results
        kditc=vars(opts)
        if opts.watch == True and opts.grid==False and opts.vis==True: wrapper(self.duringRun)
        elif opts.watch == True and opts.grid==False and opts.vis==False: self.duringRunNoVis()
        elif opts.watch == True and opts.grid==True: self.runGridWatch(opts,bt)
        else: self.nf.run()

    def getArgs(self,parser):
        c=cmptConf('/etc/crumpit.config')
        wfconfig=dict(c.config.items('crumpit'))
        cwd = os.getcwd()
        dir_path = os.path.dirname(os.path.realpath(__file__))
        parser.add_argument('-s', '--sample_name', required=True,
                                 help='Specify sample name will be used in mongoDB.')
        parser.add_argument('-f5s', '--fast5s', required=True,
                                 help='Specify directory containing fast5 files (recursive works).')
        #optionals
        parser.add_argument('-ip', '--ip', required=False, default=wfconfig['ip'],
                                 help='IP address for mongoDB database')
        parser.add_argument('-p', '--port', required=False, default=wfconfig['port'],type=int,
                                 help='port address for mongoDB database')
        parser.add_argument('-w', '--watch', required=False, action='store_true',
                                 help='to watch run, default is False')
        parser.add_argument('-vi','--vis', required=False, default=False,
                                 help='use curses visualisation (True/False)')
        #images=list_of_files = glob.glob(dir_path+'/images/*.img')
        #latest_image = max(list_of_files, key=os.path.getctime)
        parser.add_argument('-bdir', '--bdir', required=False, default='{0}/all_batches'.format(cwd),
                                 help='output directory for batch list of files')
        parser.add_argument('-bnum', '--batch_numbers', required=False, default=wfconfig['batch_numbers'],type=int,
                                 help='Number of batches to start from')
        parser.add_argument('-bs', '--batchsize', required=False, default=wfconfig['batchsize'],type=int,
                                 help='Number of files to analyse in a batch (default={0})'.format(wfconfig['batchsize']))
        parser.add_argument('-f', '--flow', required=False, default=wfconfig['flow'],
                                 help='Flowcell used (needed for basecalling only), default={0}'.format(wfconfig['flow']))
        parser.add_argument('-k', '--kit', required=False, default=wfconfig['kit'],
                                 help='kit used (needed for basecalling only) default={0}'.format(wfconfig['kit']))
        # results.args
        parser.add_argument('-c', '--cent_score', required=False, default=wfconfig['cent_score'], type=int,
                                 help='Centrifuge score cuttoff threshold. Default=150')
        parser.add_argument('-cb', '--cent_bases', required=False, default=wfconfig['cent_bases'], type=float,
                                 help='Centrifuge bases percentage cuttoff threshold. Default=10')
        parser.add_argument('-r', '--minimum_map_bases', required=False, default=wfconfig['minimum_map_bases'], type=float,
                                 help='Mininum bacterial bases percentage cuttoff threshold. Default=1')
        parser.add_argument('-q', '--mapq', required=False, default=wfconfig['mapq'], type=int,
                                 help='Mininum mapq quality threshold for mapping. Default=50')
        parser.add_argument('-ch', '--chart',required=False, default=wfconfig['chart'],
                                 help='chart on(y) or off(n), default=n')
        parser.add_argument('-ut', '--until',required=False, default=60*60*24*2, type=int,
                                 help='Time in seconds after start of run to show in chart')
        parser.add_argument('-bar', '--barcode',dest='barcode', action='store_true', default=wfconfig['barcode'],
                                 help='Use barcoding in Albacore')
    #    parser.add_argument('-nobar', '--no-barcode',dest='barcode', action='store_false',
    #                             help='Do not use barcoding in Albacore')
        parser.add_argument('-wf', '--wf', required=False, default=wfconfig['wf'],
                                 help='workflow to use')
        parser.add_argument('-img', '--img', required=False, default=wfconfig['img'],
                                 help='singularity image to use')
        parser.add_argument('-cdb', '--cdb', required=False, default=wfconfig['cdb'],
                                 help='Centrifuge database to use default={0}'.format(wfconfig['cdb']))
        parser.add_argument('-refs', '--refbase', required=False, default=wfconfig['refbase'],
                                 help='Location of reference sequences default={0}'.format(wfconfig['refbase']))
        parser.add_argument('-sc', '--scratch', required=False, default=wfconfig['scratch'],
                                 help='Location for nextflow work directory default={0}'.format(wfconfig['scratch']))
        parser.add_argument('-g', '--grid',required=False,action='store_true',
                                 help='run gridio workflow')
        parser.add_argument('-gf', '--gridFiles', required=False,default=8000,type=int,
                help='GridION fast5 batch size')
        parser.add_argument('-wh','--watchHours',required=False,default=48,
                help='Number of hours to keep watching GridION run')
        parser.add_argument('-resume', '--resume',required=False,action='store_true',
    		help='resume nextflow workflow, does not work with watch')
        parser.add_argument('-test', '--test',required=False,action='store_true',
    		help='usef for testing CRuMPIT, stores summaries in crumpit_summaries_test')
        parser.add_argument('-porechop','--porechop', required=False, default='normal',
                help='porechop parameters to use, default=normal, strict or off')
        parser.add_argument('-bc','--basecalling',required=False, default=True,
                help='basecall reads True or False, uses guppy')
        parser.add_argument('-insfx', '--insfx', required=False, default='batch',
                help='input file suffix, batch or tar')
        parser.add_argument('-map','--map',required=False, default='on',nargs='+',
                help='to perform mapping or not. Default=on (metagenic). off=no mapping. Taxids=map only reads belonging to to this taxid or lower')
        return parser


if __name__ == "__main__":
    # config file
    # args
    parser = ArgumentParser(description='CRuMPIT clinical metagenomics classification workflow')
    cc=crumpitCall()
    parser=cc.getArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    cc.run(opts)
