from pymongo import MongoClient
from Bio import SeqIO
import sys
import gzip

class mongPusher:
    def __init__(self):
        client = MongoClient('163.1.213.101', 27017)
        self.name = str(sys.argv[2])
        self.db = client[self.name]
        self.collection = self.db.alba_stats

    def getJson(self,fq):
        stats=[]
        for sq in SeqIO.parse(fq,'fastq'):
            d=str(sq.description).split(" ")
            av=(sum(sq.letter_annotations["phred_quality"])/float(len(sq.seq)))
            qs=float((1-(10**(-av/10)))*100)
            p = {"_id": str(sq.id),
                 "length": len(sq.seq), 
                 "start_time": str(d[4].split("=")[-1]),
                 "runid": str(d[1].split("=")[-1]),
                 "read_number": int(d[2].split("=")[-1]),
                 "channel": int(d[3].split("=")[-1]),
                 "percent_avq": qs,
                 "average_PHRED": av}
            stats.append(p)
        return stats

    def pushPosts(self,stats):
        posts=self.collection
        posts_id = posts.insert_many(stats)

if __name__ == "__main__":
    m=mongPusher()
    if str(sys.argv[1])[-2:] == "gz":
        fq=gzip.open(sys.argv[1])
    else:
        fq=open(sys.argv[1])
    s=m.getJson(fq)
    m.pushPosts(s)


