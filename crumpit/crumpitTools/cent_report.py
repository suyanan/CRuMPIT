#!/usr/bin/env python
from pymongo import MongoClient
import sys
from ete3 import NCBITaxa
from subprocess import Popen,PIPE
from threading import Thread
from argparse import ArgumentParser, SUPPRESS


ncbi = NCBITaxa()

class mStats:
    def __init__(self,sample_name='test',cdb='test',thresh=150,ip='127.0.0.1',port=27017,outpath='./'):
        client = MongoClient(ip, port)
        self.dbname=str(sample_name)
        self.db = client[sample_name]
        self.thresh=thresh
        self.species={}
        self.centdb=cdb
        self.barcodeInfo()
        #self.reportOut=open("{0}/{1}_kreport_score_{2}.txt".format(outpath,self.dbname,self.thresh),'wb')
        self.reportLines=[]
        self.jsons=[]

    def barcodeInfo(self):
        collection = self.db.cent_stats
        self.bars=collection.distinct('barcode')
        print(self.bars)
        if len(self.bars)==0:
            self.bars.append('nobarcode')
        #else:
        collection.create_index("barcode")
        self.reportOut={}
        for b in self.bars:
            self.reportOut[b]=open("{0}_{1}_kreport_score_{2}.txt".format(self.dbname,b,self.thresh),'wb')

    def dictString(self,d):
        return "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n".format(d['read_id'], d['seqID'], d['taxID'], d['score'], d['s2ndBestScore'], d['hitLength'], d['queryLength'], d['numMatches'])

    def allStats(self,barcode=None):
        collection = self.db.cent_stats
        if barcode==None:
            hce = collection.find()
        else:
            hce = collection.find({'barcode':barcode})
        return hce

    def cenReport(self):
        a=['centrifuge-kreport', '--min-score', str(self.thresh), '-x', self.centdb]
        self.kreport=Popen(a,stdin=PIPE, stdout=PIPE,stderr=None)

    def _sender(self,inp):
        if inp == None:
            self.kreport.stdin.close()
            return
        headermap="{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n".format('readID','seqID','taxID','score','2ndBestScore','hitLength','queryLength','numMatches')
        self.kreport.stdin.write(headermap.encode())
        for i in inp:
            self.jsons.append(i)
            self.kreport.stdin.write(self.dictString(i).encode())
        self.kreport.stdin.close()

    def _recvr(self,barcode):
        for line in self.kreport.stdout:
            self.reportLines.append(line)
            if barcode=='nobarcode':
                barcode='none'
            try:
                self.reportOut[barcode].write(line)
            except:
                pass

    def run(self,inp=None,barcode='nobarcode'):
        if len(self.bars)==1 and inp==None:
            inp=self.allStats()
        elif inp != None:
            pass
        else:
            for b in self.bars:
                barinp=self.allStats(barcode=b)
                self.run(inp=barinp,barcode=b)


        self.cenReport()
        sdr=Thread(target=self._sender,kwargs={"inp":inp})
        rcv=Thread(target=self._recvr,kwargs={"barcode":barcode})

        sdr.start()
        rcv.start()

        sdr.join(6000.0)
        rcv.join(6000.0)


############### ANALYSIS #################################
#mrgDct=getMergedTaxes()

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='Generate report from MongoDB database')
    parser.add_argument('-s', '--sample_name', required=True,
                             help='Specify sample name will be used in mongoDB.')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
    parser.add_argument('-cdb', '--cdb', required=False, default="/home/nick/dbs/p_compressed/p_compressed+h+v",
                             help='Centrifuge database to use default=/home/nick/dbs/p_compressed/p_compressed+h+v')

    parser.add_argument('-c', '--cent_score', required=False, default=150, type=int,
                             help='Centrifuge score cuttoff threshold. Default=150')

    opts, unknown_args = parser.parse_known_args()
    m=mStats(sample_name=opts.sample_name,cdb=opts.cdb,thresh=opts.cent_score,ip=opts.ip,port=opts.port)
    m.run()
