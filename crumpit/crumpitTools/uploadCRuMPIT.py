#!/usr/bin/env python3
import sys
from pymongo import MongoClient
from argparse import ArgumentParser
from crumpitTools.mongdbConnect import mdbConnect
from crumpitTools.runStats import runStats
import json
import pprint
import numpy as np

class getCrums(mdbConnect):
    def __init__(self,**kwargs):
        allowed=['json_file','crumDict','runData']
        mdbConnect.__init__(self,**kwargs)
        for k, v in kwargs.items():
            setattr(self, k, v)
        if not hasattr(self,'crumDict'):
            self.crumData()
        if not hasattr(self,'runData'):
            self.getRunData(**kwargs)
        self.allData={**self.runData,**self.crumDict}
        self.allData['sample_ID']=self.table_name

    def getDataDict(self,bar,b,q):
        '''takes runStats barcode, bases(list) and qualities(list),returns
        dictionary of compiled stats'''
        if len(b[bar]) == 0:
            return {}
        d={}
        d['1D_Mbases']=sum(b[bar])/1000000.0
        d['1D_read_no']=len(b[bar])
        d['1D_av_Qscore']=sum(q[bar])/len(b[bar])
        d['longest']=max(b[bar])
        d['shortest']=min(b[bar])
        d['median_read_length(kb)']=(np.median(b[bar]))/1000.0
        d['av_read_length(kb)']=(sum(b[bar])/len(b[bar]))/1000.0
        return d

    def crumData(self):
        '''load json file from CRuMPIT output'''
        #if hasattr(self,'json_file'):
        self.crumDict={}
        if self.json_file != None:
            self.crumDict=json.load(open(self.json_file))

    def getRunData(self,**kwargs):
        ''' get albacore data for (multiplexed)sample from hot table'''
        if 'runData' in kwargs:
            b,q,t=kwargs['runData']
            m=kwargs['runStats']
        else:
            m=runStats(**kwargs)
            b,q,t=m.getSampleStats()
        self.runData={'barcode_summaries':{}}
        ## data
        for bar in b:
            self.runData['barcode_summaries'][bar]=self.getDataDict(bar,b,q)
        reads={'total':[item for sublist in(list(b.values())) for item in sublist]}
        qs={'total':[item for sublist in(list(q.values())) for item in sublist]}
        self.runData['barcode_summaries']['total']=self.getDataDict('total',reads,qs)

        #metadata
        if len(t)==0: return
        self.runData['activeChannels']=len(m.channels)
        self.runData['run_ids']=m.run_ids
        times=sorted(list(t.keys()))
        self.runData['seq_start']=times[0]
        self.runData['seq_stop']=times[-1]
        self.runData['runtime(hours)']=times[-1]/(60*60)
        self.runData['date_started']=m.first_doc
        self.runData['channelStats']=m.channelStats
        self.allData={**self.runData,**self.crumDict}
        self.allData['sample_ID']=self.table_name

class pushCrums(mdbConnect):
    def __init__(self,**kwargs):
        mdbConnect.__init__(self,**kwargs)
        for k, v in kwargs.items():
            setattr(self, k, v)

    def pushData(self,post):
        old=self.collection.count_documents({"sample_ID":post['sample_ID']})
        #old=self.collection.find({"sample_ID":post['sample_ID']})
        if old == 1:
            posts=self.collection
            post = posts.replace_one({"sample_ID":post['sample_ID']},post)
            #print(post.matched_count)
        elif old > 1:
            pass
            #print("{0} With sample id {1}".format(old.count(),post['sample_ID']))
            #sys.exit()
        elif old == 0:
            self.pushIndividualPosts(post)
            #print("Pushed for first time")
            #sys.exit()
        else:
            pass
            #print("Nope")
            #sys.exit()

    def pushIndividualPosts(self,post):
        posts=self.collection
        post_id = posts.insert_one(post).inserted_id
        #pprint.pprint(post_id)


if __name__ == "__main__":
        # args
        parser = ArgumentParser(description='CRUMPIT to CRUMPITDB detail pusher')
        parser.add_argument('-t', '--table_name', required=False,default='MMMdbtest',
                             help='MMM_Nanopore_db is the name of the default db setup')
        parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
        parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
        parser.add_argument('-j', '--json_file',required=False,
                             help='CRUMPIT JSON file')

        opts, unknown_args = parser.parse_known_args()
        kdict=vars(opts)

        # get data from runs
        c=getCrums(**kdict)
        #pprint.pprint(c.allData)

        # push data
        pargs=kdict.copy()
        pargs['table_name']='MMMdbtest'
        p=pushCrums(**pargs)
        p.pushData(c.allData)
        #p.pushIndividualPosts(c.allData)
