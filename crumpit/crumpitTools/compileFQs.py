#!/usr/bin/env python3
import sys
import os
from pymongo import MongoClient
from argparse import ArgumentParser
from glob import glob
import gzip
from tqdm import tqdm
import subprocess

class mongPusher:
    def __init__(self,sample,ofl,ip='127.0.0.1',port=27017):
        client = MongoClient(ip, port)
        self.name = sample
        self.db = client[self.name]
        self.collection = self.db.alba_summary
        self.outfol=ofl

    def getRunMeta(self):
        self.barcodes = self.collection.distinct('barcode_arrangement')
        self.barcodes.append('')
        self.runs = self.collection.distinct('run_id')


    def writeFile(self,inF,b,r,q):
        try:
            with open(inF) as inf, gzip.open("{3}/{0}/{1}/fastq_runid_{2}_0.fastq.gz".format(q,b,r,self.outfol),"at") as outf:
                outf.writelines(inf)
        except (FileNotFoundError):
            pass

    def getFiles(self,pubfq='fastqs'):
        batches=glob("{0}/*/".format(pubfq))
        for b in self.barcodes: self.makeDirs(b)
        qs=['pass','fail']
        for batch in tqdm(range(len(batches))):
            for r in self.runs:
                for b in self.barcodes:
                    for q in qs:
                        f="{0}workspace/{3}/{1}/fastq_runid_{2}_0.fastq".format(batches[batch],b,r,q)
                        self.writeFile(f,b,r,q)

    def getSums(self,pubfq='fastqs'):
        batches=glob("{0}/*/".format(pubfq))
        with gzip.open('sequencing_summary.txt.gz','wt') as outf:
            for b in tqdm(range(len(batches))):
                with open(batches[b] + 'sequencing_summary.txt', 'rt') as inf:
                    for line in inf: outf.write(line)


    def makeDirs(self,d):
         if not os.path.exists(self.outfol + '/pass/'+ d):
             os.makedirs(self.outfol +'/pass/'+ d)
         if not os.path.exists(self.outfol + '/fail/'+ d):
             os.makedirs(self.outfol +'/fail/'+ d)



if __name__ == "__main__":
        # args
        parser = ArgumentParser(description='remove human reads and split by barcode')
        parser.add_argument('-s', '--sample_name', required=True,
                             help='Specify sample name as used in mongoDB.')
        parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
        parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
        parser.add_argument('-o', '--out', required=False, default='basecalled_fastq',type=str,
                             help='output folder for basecalled fastq files')
        parser.add_argument('-g', '--grid', required=False, action=store_true,
                            help='Specify if from GridION')

        opts, unknown_args = parser.parse_known_args()
        # run
        m=mongPusher(opts.sample_name,opts.out,ip=opts.ip,port=opts.port)
        m.getRunMeta()
        m.getFiles()
        m.getSums()
