#!/usr/bin/env python3
import sys
import json
import os
import subprocess
import pysam
from argparse import ArgumentParser, SUPPRESS
import os.path
from Bio import bgzf


# pre map
class prePile:
    def __init__(self,base,bam,out):
        self.refDict=self.load_obj(base + 'refs.json')
        self.basedir=base
        self.bam=bam
        self.taxid=os.path.basename(self.bam).split('_')[0]
        self.ref=self.basedir + self.whichRef(self.taxid)
        self.checkBgz()
        self.mpileupfile=out

    def checkBgz(self):
        if os.path.isfile(self.ref):
            self.refbgz=self.ref.replace('.gz','.bgzip')
            if not os.path.isfile(self.refbgz):
                zcat = subprocess.Popen(('zcat', self.ref), stdout=subprocess.PIPE)
                bgzip = subprocess.check_output(('bgzip'), stdin=zcat.stdout)
                zcat.wait()
                with open(self.refbgz,'wb') as outfile:
                    outfile.write(bgzip)

    def whichRef(self,taxid):
        try:
            return self.refDict[str(taxid)]
        except:
            # hack for tb
            try:
                if str(taxid)=='1763':
                    return self.refDict["83332"]
            except: return "no reference"
            return "no reference"

    def load_obj(self, name ):
        with open( name , 'r') as f:
            return json.load(f)

    def pileUp(self):
        p=pysam.mpileup("-f",self.refbgz,self.bam,"-o",self.mpileupfile)


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='prePile component for Classflow clinical metagenomics classification workflow')
    parser.add_argument('-b', '--bam',required=True,
                             help='Centrifuge raw/reduce file in gzip compression.')
    parser.add_argument('-r', '--basedir',required=True,
                             help='base directory for references used')
    parser.add_argument('-o', '--mpileupFile',required=True,
                             help='output file for mpileup')

    

    opts, unknown_args = parser.parse_known_args()

    #optionals
    opts, unknown_args = parser.parse_known_args()

    # run script
    basedir=opts.basedir + '/'
    #refDict=load_obj(basedir + 'refs.json')
    p=prePile(basedir,opts.bam,opts.mpileupFile)
    print(p.taxid,p.ref)
    p.pileUp()

