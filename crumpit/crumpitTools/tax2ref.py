import sys
import json
from argparse import ArgumentParser, SUPPRESS


def load_obj( name ):
    with open( name , 'r') as f:
        return json.load(f)

def whichRef(taxid):
    '''return reference file from json (dictionary) given taxid'''
    try:
        return refDict[taxid]
    except:
        # hack for tb
        try:
            if str(taxid)=='1763':
                return refDict["83332"]
        except: return "no reference"
        return "no reference"


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='preMap component for Classflow clinical metagenomics classification workflow')
    parser.add_argument('-b', '--basedir', required=True,
                             help='basedir for refseq json')
    parser.add_argument('-t', '--taxid',required=True,
                             help='taxid to return ref')

    opts, unknown_args = parser.parse_known_args()
    
    refDict=load_obj(opts.basedir + '/refs.json')
    print(opts.basedir + '/' + whichRef(str(opts.taxid)))


