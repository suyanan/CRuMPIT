import unittest
import os,shutil
from pymongo import MongoClient
from crumpitTools.preMap import preMapper,mongPusher,runPreMap 
from crumpitTools.map_stats import mpStats

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')
cent_file = os.path.join(data_dir, 'preMap/fuge.txt.gz20')
bovis_file = os.path.join(data_dir, 'preMap/fuge_bovis.txt.gz')
nontb_file = os.path.join(data_dir, 'preMap/nontbfuge.txt.gz')
fq_file = os.path.join(data_dir, 'preMap/high_complex.fastq20')

#refjson = os.path.join(data_dir,'preMap/refs.json's) 
basedir = os.path.join(data_dir, 'preMap/') 
#'/mnt/nanostore/dbs/refseq/'
results1={'BC01': {'1763': {'bases': 9019, 'Reads': 17}}}

class testpreMaps(unittest.TestCase):

    def setUp(self):
        self.db1='preMap_test'
        self.db2='preMapPush_test'
        self.ip = '127.0.0.1'
        self.port = 27017
        self.m=mongPusher(self.db1,ip=self.ip,port=self.port)

        self.mapq=5
        self.cent_score=1

    def testpreMapAlign(self):
        '''align read to reference'''
        pm=preMapper(basedir)
        pm.map_each(cent_file,fq_file)

        bams=pm.bamFiles
        print(bams)
        assert 'sorted/BC01/1773_0.sorted.bam' in bams
 

    def testPreMapPush(self):
        '''align then push data to mongodb'''
        pm=preMapper(basedir)
        pm.map_each(cent_file,fq_file)

        m=mongPusher(self.db2)
        m.fqs=pm.fqs

        for sam in pm.bamFiles:
            if sam==None:continue
            i=sam.split('/')
            barcode = i[1]
            taxid = i[2].split('_')[0]
            m.getData(sam,taxid)
        #print(m.posts)
        if len(m.posts) > 0: m.pushPosts()

        bars=['BC01']
        rds_needed=1
        res={}
        
        mp=mpStats(self.db2,score=self.mapq,mdbip=self.ip,mdbport=self.port,thresh=self.cent_score)
        mp.bases=float(rds_needed)
        mp.barcodes='BC01'
        mps=mp.visTimings(["1763"],showChart=False,ut=60*60*24*2)
        
        res['BC01']=mps
        print(res)
        assert res==results1


    def test_topMyco(self):
        pm=preMapper(basedir)
        print('default tb ref: ',pm.tbref)
        assert pm.tbref=='83332'
        
        pm=preMapper(basedir)
        pm.topMyco(cent_file)
        print('standard tb ref:', pm.tbref)
        # test standard tb
        assert pm.tbref == '1773'

        # test Mbovis
        pm=preMapper(basedir)
        pm.topMyco(bovis_file)
        print('bovis tb ref:', pm.tbref)
        assert pm.tbref == '1765'

        # test nontb
        pm=preMapper(basedir)
        pm.topMyco(nontb_file)
        print('nontb tb ref:', pm.tbref)
        assert pm.tbref == '83332'



    def tearDown(self):
        client = MongoClient(self.ip, self.port)
        client.drop_database(self.db1)
        client.drop_database(self.db2)




