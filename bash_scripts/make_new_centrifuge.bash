
refs=/mnt/Data2/dbs/refseq

d=$(date -I)
library='centrifuge_'$d
echo $library

mkdir $library
cd $library

#centrifuge-download -o library \
#	-d vertebrate_mammalian \
#	-a "Chromosome" \
#	-t 9606 -c \
#	'reference genome' \
#	refseq >> ${refs}/human_conversion_table.txt

domains="human bacteria viral"

for domain in ${domains}
do
    echo "dustmasking: " ${domain}
    zcat $refs/${domain}/*.fna.gz | dustmasker \
	    -infmt fasta \
	    -in - -level 20 \
	    -outfmt fasta -out - \
	    >> input-sequences.fna
    cat $refs/${domain}_conversion_table.txt >> seqid2taxid.map
done

echo "downloading tax"
centrifuge-download -o taxonomy taxonomy


echo "building!"
centrifuge-build -p 6 --conversion-table seqid2taxid.map \
                 --taxonomy-tree taxonomy/nodes.dmp \
		 --name-table taxonomy/names.dmp \
                 input-sequences.fna \
		 $library

