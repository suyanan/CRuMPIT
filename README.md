# Clinincal Real-time Metagenomics Pathogen Identification Test (CRuMPIT)
Clinical metagenomic workflow for monitoring and analysing Oxford Nanopore MinION runs. Uses centrifuge for initial classification and minimap2 for species confirmation. Uses Nextflow (https://www.nextflow.io/) and can be run in Docker or singularity.

crumpit requires nextflow and mongoDB(https://docs.mongodb.com/manual/administration/install-community/) to be installed.
I am currently updating this documentation, therefore parts may not work.

## Installation
The running of crumpit is done through a few scripts, which in turn use singularity
and nextflow. The package can be installed like this as below.
Then the image needs to be made (or downloaded), a reference folder downloaded
and a config file needs to be completed.

	git clone https://gitlab.com/ModernisingMedicalMicrobiology/CRuMPIT.git
	cd CRuMPIT
	sudo python3 setup.py install
	sudo python3 setup.py develop


### Singularity container build
I've used docker to create a singulairity image from a docker file. This way I don't need root access to run on a cluster. The bash scripts for this are also available in the 'bash_scripts' folder.

To build the docker image (ensure you have docker installed etc etc with permissions and stuff).
This can be done on a machine you have permissions on and moved to another machine
when you have the flat singularity image file.

	docker build -t crumpit .

Then to convert to singularity image, use this docker image and change "/path/to/" to a location you want to keep the image. You will also need to move it to a location all nodes can see if using a cluster.

	docker run \
	 -v /var/run/docker.sock:/var/run/docker.sock \
	 -v /path/to/:/output \
	 --privileged -t --rm \
	 singularityware/docker2singularity \
	 crumpit

For guppy installation, both guppy and ont_core_cpp repos need to be cloned into a directory called 3rd_party then run the Dockerfile_guppy. You will also need an Nvidia graphics card with CUDA for this to work.

	docker build -t crumpit_guppy -f Dockerfile_guppy .

Then then same for singularity.

	docker run \
	 -v /var/run/docker.sock:/var/run/docker.sock \
	 -v /path/to/:/output \
	 --privileged -t --rm \
	 singularityware/docker2singularity \
	 crumpit_guppy


### Download references
Make a directory you want to contain the reference genome files.
This will need to be accessible by all nodes if using a cluster.

Run the script. If connections are lost, rerun and it will pick up from before.

	crumpit refMaker -d bacteria -download -ct

This can take a while to perform depending on your internet connection.

### Config file
Edit the config file in configs/crumpit.config and move it to /etc/

## Running

## Running the workflow
Running the pipeline can be split into three separate parts. (A) "Run management" to watch the run and create batch jobs for (B) "nextflow workflow" which conducts the data processing and can be scaled in a cluster etc, and (C) Run analysis which can be run in real time to monitor results from finished batch jobs uploaded to the database. I've created the convenience program "crumpit_call.py" that combines the three and steps and outputs information to a curses window (I know, I know curses is terrible but its a means to an ends here).
This is the most basic way to run crumpit. Use help (-h) to see all options.

	crumpit call -s SAMPLE_NAME -f5s /dir/containing/fast5s --watch

### (A) Run management
Here the folder ("/mnt/minion2_data/reads/") is monitored by getFiles and batches are output into the folder "batches". The number of previous batches here is 0, change to the number of previously found batches in the event of a crash or restart.

	crumpit batch /mnt/minion2_data/reads/ batches/ 0

### (B) Nextflow workflow

	nextflow run workflows/PJI_workflow_with_albacore.nf -with-singularity /path/to/crumpit-date_etc.img  -w /work/SCRATCH/

### (C) Run analysis
This is currently a collection of scripts that connect to the mongodb and make some graphs. In the future is will probably web based.

	crumpit results -s SAMPLE_NAME

## GridION
For running basecalled (guppy) samples from the GridION. Use the '--grid' option.
Also specify the workflow. For example

	crumpit call \
	-s ${sample} \
	-f5s /mnt/grid0/basecaled/lambda \
	--barcode \
	--watch \
	--grid \
	--wf /workflows/PJI_workflow_gridion_watch.nf

This won't produce the gui as it's going to be run automatically. I might add this is
as an option. But it will produce the results .json file. Future updates will push
this into a summary database automatically,

### Automated GridION processing
This script can be added as a cron job and will check for new plates and run them
through CRuMPIT if they have not already need run. Each plate with run for 48 hours
automatically picking up new fastq files. The time can be changed with '-wh 48'.

	checkGrid.py \
	-d /mnt/grid0/basecaled/ \
	-workfol /work/basecalled

It will generate a logFile.json with each run previously processed or under process.

	{
	"lambda":
		{
		"status": "Finished",
		"PID": 19341,
		"starttime": [2018, 3, 6, 12, 34, 28, 1, 65, 0],
		"cwd": "/work/basecalling/lambda",
		"Finishedtime": [2018, 3, 6, 12, 45, 5, 1, 65, 0],
		"Submittedtime": [2018, 3, 6, 12, 34, 28, 1, 65, 0]
		},
	"lambda_2":
		{
		"status": "Finished",
		"PID": 14233,
		"starttime": [2018, 3, 6, 12, 9, 17, 1, 65, 0],
		"cwd": "/work/basecalling/lambda_2",
		"Finishedtime": [2018, 3, 6, 12, 19, 35, 1, 65, 0],
		"Submittedtime": [2018, 3, 6, 12, 9, 17, 1, 65, 0]
		}
	}
